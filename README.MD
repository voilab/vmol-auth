# @voilab/vmol-auth [![NPM version](https://img.shields.io/npm/v/@voilab/vmol-auth.svg)](https://www.npmjs.com/package/@voilab/vmol-auth)

This package is part of the vmol collection.

Moleculer middlewares providing authentication and authorization features.

## Features

- JWT based authentication
- Handle user (interactive), application (machine) and anonymous (public) authentication
- Casl based authorization
- Mutli roles and multi tenants support
- Highly configurable
- Fully tested

## Install

```bash
npm install @voilab/vmol-auth --save
```

## Authentication Middleware

### Authentication Configuration

| Property | Type | Default | Description |
|-|-|-|-|
| `userJWTParamsAction` | `String` | `'authentication.getJWTParams'` | The action name to call to get the user JWT params. |
| `appAuthGetPublicKeyAction` | `String` | `'authentication.getApplicationPublicKey'` | The action name to call to get the public key for application authentication. |
| `jwtClockTolerance` | `Number` | `30` | The clock tolerance for JWT verification. |
| `jwtParamsTTL` | `String` | `'1h'` | The time-to-live for the JWT parameters cache. |
| `applicationKeysCacheSize` | `Number` | `128` | The maximum size of the application keys cache. |
| `applicationKeysCacheTTL` | `String` | `'1d'` | The time-to-live for the application keys cache. |
| `applicationJwtClockTolerance` | `Number` | `30` | The clock tolerance for application JWT verification. |
| `applicationJwtMaxAge` | `String` | `'30m'` | The maximum age for application JWT verification. |
| `slugSeparator` | `String` | `':'` | The separator for the authentication slug. |
| `serviceBlacklist` | `Array.<String>` | `[]` | The list of services to skip the authentication middleware. The `$node` service is always added to the blacklist. |

## Authorization Middleware

### Authorization Configuration

| Property | Type | Default | Description |
|-|-|-|-|
| `abilitiesForRoleAction` | `String` | `'authorization.getAbilitiesForRole'` | The action name to call to get the abilities for a role. |
| `defaultAbilitiesAction` | `String` | `'authorization.getDefaultAbilities'` | The action name to call to get the default abilities. |
| `applicationRolesAction` | `String` | `'authorization.getApplicationRoles'` | The action name to call to get the roles for an application. |
| `applicationRolesActionParam` | `String` | `'id'` | The parameter name containing the application ID. |
| `applicationRolesActionSecret` | `String` | `null` | The secret to send with the request. |
| `userRolesAction` | `String` | `'authorization.getUserRoles'` | The action name to call to get the roles for a user. |
| `userRolesActionParam` | `String` | `'id'` | The parameter name containing the user ID. |
| `userRolesActionSecret` | `String` | `null` | The secret to send with the request. |
| `anonymousRolesAction` | `String` | `'authorization.getAnonymousRoles'` | The action name to call to get the anonymous roles. |
| `entitiesRolesCacheMax` | `Number` | `128` | The maximum number of entities to cache roles for. |
| `entitiesRolesCacheTTL` | `String` | `'1h'` | The time-to-live for the entities roles cache. |
| `roleAbilitiesCacheMax` | `Number` | `128` | The maximum number of roles to cache abilities for. |
| `roleAbilitiesCacheTTL` | `String` | `'1h'` | The time-to-live for the role abilities cache. |
| `compiledAbilitiesCacheMax` | `Number` | `128` | The maximum number of role combinations to cache compiled abilities for. |
| `compiledAbilitiesCacheTTL` | `String` | `'1h'` | The time-to-live for the compiled abilities cache. |
| `serviceBlacklist` | `Array.<String>` | `[]` | The list of services to skip the authorization middleware. The `$node` service is always added to the blacklist. |

## v0.6.0 Migration

Both mxins have been replaced by middlewares.

### `moleculer.config.js`

Require the middlewares and add them to the middlewares array.
Take care of the order, the authentication middleware must be called before the authorization middleware.
Which means that the authentication middleware must be **after** the authorization middleware in the array.
This because the middleware are called in the reverse order.

```diff
+const AuthorizationMiddleware = require('@voilab/vmol-auth').Authorization;
+const AuthenticationMiddleware = require('@voilab/vmol-auth').Authentication;

module.exports = {
    // Register custom middlewares
    middlewares: [
+        AuthorizationMiddleware({
+            userRolesAction: 'myservice.getRolesForUser',
+            userRolesActionSecret: process.env.MY_ACTION_SECRET
+        }),
+        AuthenticationMiddleware({
+            userJWTParamsAction: 'myservice.getJWTParamsPublic'
+        })
    ]
}
```

### Services

The old mixins must be removed from the services.

```diff
-const { AuthorizationMixin, AuthenticationMixin } = require('@voilab/vmol-auth');
-
module.exports = {
    name: 'myservice',

-    mixins: [AuthorizationMixin, AuthenticationMixin],
-
    settings: {
        foo: 'bar',
-        auth: {
-            tokenService: 'authservice',
-            abilityService: 'authservice'
-        }
    },

    /**
     * Actions
     */
    actions: {}
};
```

### Public actions

Actions that does not require authentication and/or authorization need the `disableAuthentication` and/or `disableAuthorization` options.

```javascript
module.exports = {
    name: 'myservice',

    settings: {
        foo: 'bar'
    },

    actions: {
        publicAction: {
            disableAuthentication: true,
            disableAuthorization: true,

            handler(ctx) {
                return 'Hello world!';
            }
        }
    }
};
```

## Tests

```javascript
npm run test
```

## License

This project is under [MIT License](https://opensource.org/licenses/MIT).

## Author

This package has been created by [Voilab](https://www.voilab.ch).

## Copyright

Copyright (c) 2013-2024, Voilab SNC
