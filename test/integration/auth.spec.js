const { generateKeyPairSync } = require('crypto');

const ServiceBroker = require('moleculer').ServiceBroker;

const Authorization = require('../../src/authorization.middleware');
const Authentication = require('../../src/authentication.middleware');

const jwt = require('jsonwebtoken');

function generateKeyPair() {
    return generateKeyPairSync('ec', {
        namedCurve: 'prime256v1', // ES256
        publicKeyEncoding: {
            type: 'spki',
            format: 'pem'
        },
        privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem'
        }
    });
}

describe('Test "auth" service', () => {
    const logger = {
        info: jest.fn(),
        warn: jest.fn(),
        error: jest.fn(),
        debug: jest.fn()
    };

    const svcName = 'authorization';

    const abilitiesByRole = {
        admin:  [
            [ svcName, 'privateAction']
        ],
        user: [
            [ svcName, 'action' ],
            [ svcName, 'manualCheckAction' ]
        ],
        application: [
            [ svcName, 'action' ],
            [ svcName, 'privateAction' ]
        ]
    };

    const defaultAbilities = [
        [ svcName, 'action' ]
    ];

    const jwtKeys = generateKeyPair();

    const jwtParams = {
        publicKey: jwtKeys.publicKey,
        issuer: svcName,
    };

    const actionHandler = jest.fn(() => Promise.resolve('Result'));
    const publicActionHandler = jest.fn(() => Promise.resolve('Result'));
    const privateActionHandler = jest.fn(() => Promise.resolve('Result'));
    const denyActionHandler = jest.fn(() => Promise.resolve('Result'));

    const getAbilitiesForRoleHandler = jest.fn((ctx) => Promise.resolve(abilitiesByRole[ctx.params.role]));
    const getDefaultAbilitiesHandler = jest.fn(() => Promise.resolve(defaultAbilities));
    const getRolesForApplicationHandler = jest.fn(() => Promise.resolve({'application': ['tenant-1']}));
    const getRolesForUserHandler = jest.fn(() => Promise.resolve({'user': ['tenant-1', 'tenant-2'], 'admin': ['tenant-2']}));
    const getDefaultRolesHandler = jest.fn(() => Promise.resolve({}));
    const actionGetJWTParamsHandler = jest.fn(() => Promise.resolve(jwtParams));

    const serviceDefinition = {
        name: svcName,

        actions: {
            action: actionHandler,
            publicAction: {
                disableAuthorization: true,
                handler: publicActionHandler
            },
            privateAction: privateActionHandler,
            denyAction: denyActionHandler,

            getAbilitiesForRole: {
                disableAuthorization: true,
                handler: getAbilitiesForRoleHandler
            },
            getDefaultAbilities: {
                disableAuthorization: true,
                handler: getDefaultAbilitiesHandler
            },
            getRolesForApplication: {
                disableAuthorization: true,
                handler: getRolesForApplicationHandler
            },
            getRolesForUser: {
                disableAuthorization: true,
                handler: getRolesForUserHandler
            },
            getDefaultRoles: {
                disableAuthorization: true,
                handler: getDefaultRolesHandler
            },
            getJWTParams: {
                disableAuthorization: true,
                handler: actionGetJWTParamsHandler
            }
        }
    };

    async function createService(authz = true, authn = true) {
        const middlewares = [];
        if (authz) {
            middlewares.push(Authorization({
                logger
            }));
        }

        if (authn) {
            middlewares.push(Authentication({
                logger,
                userJWTParamsAction: `${svcName}.getJWTParams`,
            }));
        }

        const broker = new ServiceBroker({
            middlewares,
            nodeID: 'test',
            logger: null
        });

        broker.createService(serviceDefinition);

        await broker.start();

        return broker;
    }

    function signToken(userData, privateKey, options = {}) {
        return jwt.sign(
            userData,
            privateKey,
            {
                algorithm: 'ES256',
                issuer: jwtParams.issuer,
                ...options,
            }
        );
    }

    describe('Deprecated features', () => {
        const userData = {
            email: 'user@example.com',
            ses: '32850aef-66a1-410c-9f0c-744deb3b52ff',
            sub: '86071c4f-9f7a-493b-9275-0250712e59e8',
            roles: {
                user: ['tenant-1', 'tenant-2'],
                admin: ['tenant-2']
            }
        };

        const token = signToken(userData, jwtKeys.privateKey);

        beforeEach(() => {
            actionHandler.mockClear();
        });

        it('should register the deprecated `ctx.locals.token.roles` getter', async () => {
            const broker = await createService();

            await broker.call(`${svcName}.action`, {}, { meta: { token } });

            const ctx = actionHandler.mock.calls[0][0];
            expect(Object.isFrozen(ctx.locals.authn)).toBe(true);
            expect(Object.isFrozen(ctx.locals.authz)).toBe(true);
            expect(ctx.locals.token.roles).toEqual(userData.roles);

            await broker.stop();
        });

        it('should log a deprecation warning when using `ctx.locals.token.roles`', async () => {
            const broker = await createService();

            await broker.call(`${svcName}.action`, {}, { meta: { token } });

            const ctx = actionHandler.mock.calls[0][0];
            expect(ctx.locals.token.roles).toEqual(userData.roles);

            expect(logger.warn).toHaveBeenCalledWith(expect.stringMatching('`ctx.locals.token.roles` property is deprecated'));

            await broker.stop();
        });

        it('should log an error when the authorization middleware is not registered', async () => {
            const broker = await createService(false);

            await broker.call(`${svcName}.action`, {}, { meta: { token } });

            const ctx = actionHandler.mock.calls[0][0];
            expect(ctx.locals.authz).toBeUndefined();
            expect(ctx.locals.token.roles).toEqual({});

            expect(logger.error).toHaveBeenCalledWith(expect.stringMatching('`ctx.locals.authz` object is not available'));

            await broker.stop();
        });
    });
});
