const ServiceBroker = require('moleculer').ServiceBroker;
const { MoleculerServerError } = require('moleculer').Errors;

const { LRUCache } = require('lru-cache');
jest.mock('lru-cache');

const Middleware = require('../../../src/authorization.middleware');
const { detectSubjectType } = require('../../../lib/casl-utilities');

describe('Authorization middleware', () => {
    function createBroker(opts) {
        const broker = new ServiceBroker({
            ...opts,
            nodeID: 'test',
            logger: false
        });

        return broker;
    }

    function createMW(opts = {}) {
        const broker = createBroker();
        const mw = Middleware(opts);
        mw.created(broker);
        return mw;
    }

    const logger = {
        info: jest.fn(),
        warn: jest.fn(),
        error: jest.fn(),
        debug: jest.fn()
    };

    const svcName = 'authorization';

    const abilitiesByRole = {
        admin:  [
            [ svcName, 'privateAction']
        ],
        user: [
            [ svcName, 'action' ],
            [ svcName, 'manualCheckAction' ]
        ],
        application: [
            [ svcName, 'action' ],
            [ svcName, 'privateAction' ]
        ]
    };

    const defaultAbilities = [
        [ svcName, 'action' ]
    ];

    const actionHandler = jest.fn(() => Promise.resolve('Result'));
    const publicActionHandler = jest.fn(() => Promise.resolve('Result'));
    const privateActionHandler = jest.fn(() => Promise.resolve('Result'));
    const denyActionHandler = jest.fn(() => Promise.resolve('Result'));
    const manualCheckActionHandler = jest.fn(() => Promise.resolve('Result'));

    const getAbilitiesForRoleHandler = jest.fn((ctx) => Promise.resolve(abilitiesByRole[ctx.params.role]));
    const getDefaultAbilitiesHandler = jest.fn(() => Promise.resolve(defaultAbilities));
    const getRolesForApplicationHandler = jest.fn(() => Promise.resolve({'application': ['tenant-1']}));
    const getRolesForUserHandler = jest.fn(() => Promise.resolve({'user': ['tenant-1', 'tenant-2'], 'admin': ['tenant-2']}));
    const getRolesForAnonymousHandler = jest.fn(() => Promise.resolve({}));

    const serviceDefinition = {
        name: svcName,

        actions: {
            action: actionHandler,
            publicAction: {
                disableAuthorization: true,
                handler: publicActionHandler
            },
            privateAction: privateActionHandler,
            denyAction: denyActionHandler,
            manualCheckAction: {
                authManualACL: true,
                handler: manualCheckActionHandler
            },

            getAbilitiesForRole: {
                disableAuthorization: true,
                handler: getAbilitiesForRoleHandler
            },
            getDefaultAbilities: {
                disableAuthorization: true,
                handler: getDefaultAbilitiesHandler
            },
            getRolesForApplication: {
                disableAuthorization: true,
                handler: getRolesForApplicationHandler
            },
            getRolesForUser: {
                disableAuthorization: true,
                handler: getRolesForUserHandler
            },
            getRolesForAnonymous: {
                disableAuthorization: true,
                handler: getRolesForAnonymousHandler
            }
        }
    };

    const authnUser = {
        kind: 'user',
        id: '657538014677958657',
        email: 'accessing@mars105.net',
        sessid: '945451993715245059',
        slug: 'user/657538014677958657/945451993715245059'
    };

    const authnApp = {
        kind: 'application',
        id: '906bb924-3027-4730-a98f-333072d73da7',
        slug: 'application/906bb924-3027-4730-a98f-333072d73da7'
    };

    const authnAnon = {
        kind: 'anonymous',
        slug: 'anonymous'
    };

    function authnMockMiddleware(authn = null) {
        return {
            name: 'authn-mock',
            localAction(next) {
                return (ctx) => {
                    if (authn !== null) {
                        ctx.locals.authn = authn;
                    }

                    if (typeof ctx.locals.authn === 'object') {
                        Object.freeze(ctx.locals.authn);
                    }

                    return next(ctx);
                };
            }
        };
    }

    async function createService(opts = {}, authn = null, internalServices = false) {
        const broker = createBroker({
            internalServices,
            middlewares: [
                Middleware({
                    abilitiesChangedEvent: 'abilitiesChangedEvent',
                    applicationRolesChangedEvent: 'applicationRolesChangedEvent',
                    userRolesChangedEvent: 'userRolesChangedEvent',
                    anonymousRolesChangedEvent: 'anonymousRolesChangedEvent',
                    logger,
                    ...opts
                }),
                authnMockMiddleware(authn)
            ]
        });

        broker.createService(serviceDefinition);

        await broker.start();

        return broker;
    }

    describe('Middleware creation', () => {
        it('should create a service with the default middleware options', () => {
            expect(() => createMW()).not.toThrow();
        });

        it('should create a service with `anonymousRolesAction` set to `null`', () => {
            expect(() => createMW({
                anonymousRolesAction: null
            })).not.toThrow();
        });

        it('should throw on creation when `abilitiesForRoleAction` parameter is invalid', () => {
            expect(() => createMW({
                abilitiesForRoleAction: null
            })).toThrow('abilitiesForRoleAction');
        });

        it('should throw on creation when `defaultAbilitiesAction` parameter is invalid', () => {
            expect(() => createMW({
                defaultAbilitiesAction: null
            })).toThrow('defaultAbilitiesAction');
        });

        it('should throw on creation when `applicationRolesAction` parameter is invalid', () => {
            expect(() => createMW({
                applicationRolesAction: null
            })).toThrow('applicationRolesAction');
        });

        it('should throw on creation when `userRolesAction` parameter is invalid', () => {
            expect(() => createMW({
                userRolesAction: null
            })).toThrow('userRolesAction');
        });

        it('should throw on creation when `anonymousRolesAction` parameter is invalid', () => {
            expect(() => createMW({
                anonymousRolesAction: 0
            })).toThrow('anonymousRolesAction');
        });

        it('should throw on creation when `entitiesRolesCacheMax` parameter is invalid', () => {
            expect(() => createMW({
                entitiesRolesCacheMax: 'abcd'
            })).toThrow('entitiesRolesCacheMax');
        });

        it('should throw on creation when `entitiesRolesCacheTTL` parameter is invalid', () => {
            expect(() => createMW({
                entitiesRolesCacheTTL: null
            })).toThrow('entitiesRolesCacheTTL');
        });

        it('should throw on creation when `roleAbilitiesCacheMax` parameter is invalid', () => {
            expect(() => createMW({
                roleAbilitiesCacheMax: 'abcd'
            })).toThrow('roleAbilitiesCacheMax');
        });

        it('should throw on creation when `roleAbilitiesCacheTTL` parameter is invalid', () => {
            expect(() => createMW({
                roleAbilitiesCacheTTL: null
            })).toThrow('roleAbilitiesCacheTTL');
        });

        it('should throw on creation when `compiledAbilitiesCacheMax` parameter is invalid', () => {
            expect(() => createMW({
                compiledAbilitiesCacheMax: 'abcd'
            })).toThrow('compiledAbilitiesCacheMax');
        });

        it('should throw on creation when `compiledAbilitiesCacheTTL` parameter is invalid', () => {
            expect(() => createMW({
                compiledAbilitiesCacheTTL: null
            })).toThrow('compiledAbilitiesCacheTTL');
        });

        it('should register hooks', () => {
            const mw = createMW();

            expect(mw.created).toBeInstanceOf(Function);
            expect(mw.starting).toBeInstanceOf(Function);
            expect(mw.stopped).toBeInstanceOf(Function);
            expect(mw.localAction).toBeInstanceOf(Function);
        });
    });

    describe('Service lifecycle', () => {
        beforeEach(() => {
            LRUCache.mockClear();
        });

        it('should initialize the caches with default parameters', async () => {
            await createService();

            expect(LRUCache).toHaveBeenCalledTimes(3);
            expect(LRUCache.mock.calls[0][0]).toEqual({
                max: 8192,
                ttl: 4 * 60 * 60 * 1000 // 4 hour
            });
            expect(LRUCache.mock.calls[1][0]).toEqual({
                max: 8,
                ttl: 24 * 60 * 60 * 1000 // 1 day
            });
            expect(LRUCache.mock.calls[2][0]).toEqual({
                max: 32,
                ttl: 24 * 60 * 60 * 1000 // 1 day
            });
        });

        it('should initialize the caches with custom parameters', async () => {
            await createService({
                entitiesRolesCacheMax: 1,
                entitiesRolesCacheTTL: '1h',

                roleAbilitiesCacheMax: 2,
                roleAbilitiesCacheTTL: '2h',

                compiledAbilitiesCacheMax: 3,
                compiledAbilitiesCacheTTL: '3h',
            });

            expect(LRUCache).toHaveBeenCalledTimes(3);
            expect(LRUCache.mock.calls[0][0]).toEqual({
                max: 1,
                ttl: 60 * 60 * 1000 // 1 hour
            });
            expect(LRUCache.mock.calls[1][0]).toEqual({
                max: 2,
                ttl: 2 * 60 * 60 * 1000 // 2 hours
            });
            expect(LRUCache.mock.calls[2][0]).toEqual({
                max: 3,
                ttl: 3 * 60 * 60 * 1000 // 3 hours
            });
        });

        it('should not initialize caches for the $node internal service', async () => {
            await createService({
                serviceBlacklist: []
            }, {}, true);

            expect(LRUCache).toHaveBeenCalledTimes(3);
        });

        it('should clear caches on broker stop', async () => {
            const broker = await createService();

            expect(LRUCache).toHaveBeenCalledTimes(3);
            expect(LRUCache.mock.instances[0].clear).not.toHaveBeenCalled();
            expect(LRUCache.mock.instances[1].clear).not.toHaveBeenCalled();
            expect(LRUCache.mock.instances[2].clear).not.toHaveBeenCalled();

            await broker.stop();

            expect(LRUCache.mock.instances[0].clear).toHaveBeenCalledTimes(1);
            expect(LRUCache.mock.instances[1].clear).toHaveBeenCalledTimes(1);
            expect(LRUCache.mock.instances[2].clear).toHaveBeenCalledTimes(1);
        });
    });

    describe('Local action wrapper', () => {
        const broker = createBroker();
        const mw = createMW();

        const handler = jest.fn(() => Promise.resolve('Result'));
        const action = {
            service: {
                name: svcName
            },
            name: 'action',
            handler
        };

        afterAll(() => {
            delete action.disableAuthentication;
            action.service.name = svcName;
        });

        it('should wrap actions', async () => {
            const newHandler = mw.localAction.call(broker, handler, action);
            expect(newHandler).not.toBe(handler);
        });

        it('should not wrap actions on blacklisted services', async () => {
            action.service.name = '$node';
            const newHandler = mw.localAction.call(broker, handler, action);
            expect(newHandler).toBe(handler);
        });

        it('should not wrap actions that have the `disableAuthorization` flag', async () => {
            action.disableAuthorization = true;
            const newHandler = mw.localAction.call(broker, handler, action);
            expect(newHandler).toBe(handler);
        });
    });

    describe('Authorization', () => {
        beforeEach(() => {
            actionHandler.mockClear();
            publicActionHandler.mockClear();
            privateActionHandler.mockClear();
            manualCheckActionHandler.mockClear();
        });

        it('should throw if the `authn` object is missing', async () => {
            const broker = await createService({}, null);
            await expect(broker.call(`${svcName}.action`))
                .rejects.toThrow(MoleculerServerError);
        });

        it('should throw if the `authn` object is malformed', async () => {
            const broker = await createService({}, {foo: 'bar'});
            await expect(broker.call(`${svcName}.action`))
                .rejects.toThrow(MoleculerServerError);
        });

        it('should accept a valid `authn` object', async () => {
            const broker = await createService({}, authnUser);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');
        });

        it('should register the `authz` object in the context', async () => {
            const broker = await createService({}, authnUser);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(actionHandler).toHaveBeenCalledTimes(1);

            const ctx = actionHandler.mock.calls[0][0];
            expect(ctx.locals.authz).toBeInstanceOf(Object);
            expect(Object.isFrozen(ctx.locals.authz)).toBe(true);
            expect(ctx.locals.authz.getRoles).toBeInstanceOf(Function);
            expect(ctx.locals.authz.getRoleNames).toBeInstanceOf(Function);
            expect(ctx.locals.authz.getTenantIds).toBeInstanceOf(Function);
            expect(ctx.locals.authz.hasRole).toBeInstanceOf(Function);
            expect(ctx.locals.authz.can).toBeInstanceOf(Function);
            expect(ctx.locals.authz.throwIfACLNotChecked).toBeInstanceOf(Function);
        });

        it('should provide the `authz.can()` method', async () => {
            const broker = await createService({}, authnUser);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(actionHandler).toHaveBeenCalledTimes(1);

            const ctx = actionHandler.mock.calls[0][0];
            expect(ctx.locals.authz.can('action', svcName)).toBe(true);
            expect(ctx.locals.authz.can('privateAction', svcName)).toBe(true);
            expect(ctx.locals.authz.can('action', 'unknown')).toBe(false);
        });

        it('should provide the `authz.rulesFor()` method', async () => {
            const broker = await createService({}, authnUser);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(actionHandler).toHaveBeenCalledTimes(1);

            const ctx = actionHandler.mock.calls[0][0];
            expect(ctx.locals.authz.rulesFor('action', svcName)).toMatchSnapshot();
        });

        it('should throw when given an invalid auth kind', async () => {
            const broker = await createService({}, {kind: 'invalid'});
            await expect(broker.call(`${svcName}.action`))
                .rejects.toThrow('Unknown authentication kind');
        });

        it('should throw when calling `authz.hasRole` with invalid tenant ids', async () => {
            const broker = await createService({}, authnUser);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(actionHandler).toHaveBeenCalledTimes(1);

            const ctx = actionHandler.mock.calls[0][0];
            expect(ctx.locals.authz.hasRole).toBeInstanceOf(Function);
            expect(() => ctx.locals.authz.hasRole('user')).toThrow();
            expect(() => ctx.locals.authz.hasRole('user', null)).toThrow();
            expect(() => ctx.locals.authz.hasRole('user', 1)).toThrow();
            expect(() => ctx.locals.authz.hasRole('user', '')).toThrow();
            expect(() => ctx.locals.authz.hasRole('user', [])).toThrow();
        });

        it('should throw when ACL are not checked with `authManualACL` set to `true`', async () => {
            const broker = await createService({}, authnUser);
            await expect(broker.call(`${svcName}.manualCheckAction`))
                .rejects.toThrow('Forbidden');
        });
    });

    describe('Authorization with anonymous user', () => {
        beforeEach(() => {
            actionHandler.mockClear();
            publicActionHandler.mockClear();
            privateActionHandler.mockClear();
        });

        it('should allow access to public actions', async () => {
            const broker = await createService({}, authnAnon);
            const res = await broker.call(`${svcName}.publicAction`);
            expect(res).toBe('Result');
        });

        it('should allow `anonymousRolesAction` as `null`', async () => {
            const broker = await createService({
                anonymousRolesAction: null
            }, authnAnon);
            await expect(broker.call(`${svcName}.privateAction`))
                .rejects.toThrow('Unauthorized');
            expect(getRolesForAnonymousHandler).not.toHaveBeenCalled();
        });

        it('should deny access to private actions', async () => {
            const broker = await createService({}, authnAnon);
            await expect(broker.call(`${svcName}.privateAction`))
                .rejects.toThrow('Unauthorized');
        });

        it('should have no roles', async () => {
            const broker = await createService({}, authnAnon);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(actionHandler).toHaveBeenCalledTimes(1);

            const ctx = actionHandler.mock.calls[0][0];
            expect(ctx.locals.authz.getRoles).toBeInstanceOf(Function);
            expect(ctx.locals.authz.getRoles()).toEqual({});
            expect(ctx.locals.authz.getRoleNames).toBeInstanceOf(Function);
            expect(ctx.locals.authz.getRoleNames()).toEqual([]);
            expect(ctx.locals.authz.getTenantIds).toBeInstanceOf(Function);
            expect(ctx.locals.authz.getTenantIds()).toEqual([]);
            expect(ctx.locals.authz.hasRole).toBeInstanceOf(Function);
            expect(ctx.locals.authz.hasRole('user')).toBe(false);
            expect(ctx.locals.authz.hasRole('admin')).toBe(false);
            expect(ctx.locals.authz.hasRole('application')).toBe(false);
        });
    });

    describe('Authorization with user', () => {
        beforeEach(() => {
            actionHandler.mockClear();
            publicActionHandler.mockClear();
            privateActionHandler.mockClear();
            getRolesForAnonymousHandler.mockClear();
            getRolesForUserHandler.mockClear();
        });

        it('should allow access to public actions', async () => {
            const broker = await createService({}, authnUser);
            const res = await broker.call(`${svcName}.publicAction`);
            expect(res).toBe('Result');
        });

        it('should allow access to private actions', async () => {
            const broker = await createService({}, authnUser);
            const res = await broker.call(`${svcName}.privateAction`);
            expect(res).toBe('Result');
        });

        it('should deny access to denied actions', async () => {
            const broker = await createService({}, authnUser);
            await expect(broker.call(`${svcName}.denyAction`))
                .rejects.toThrow('Forbidden');
        });

        it('should call `getRolesForUser` action with the default parameter', async () => {
            const broker = await createService({}, authnUser);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(getRolesForUserHandler).toHaveBeenCalledTimes(1);
            expect(getRolesForUserHandler.mock.calls[0][0].params).toEqual({id: authnUser.id});
        });

        it('should call `getRolesForUser` action with a custom parameter', async () => {
            const broker = await createService({ userRolesActionParam: 'custom' }, authnUser);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(getRolesForUserHandler).toHaveBeenCalledTimes(1);
            expect(getRolesForUserHandler.mock.calls[0][0].params).toEqual({custom: authnUser.id});
        });

        it('should call `getRolesForUser` action with the secret parameter if set', async () => {
            const broker = await createService({ userRolesActionSecret: 'secret' }, authnUser);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(getRolesForUserHandler).toHaveBeenCalledTimes(1);
            expect(getRolesForUserHandler.mock.calls[0][0].params).toEqual({id: authnUser.id, secret: 'secret'});
        });

        it('should have user\'s roles', async () => {
            const broker = await createService({}, authnUser);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(actionHandler).toHaveBeenCalledTimes(1);

            const ctx = actionHandler.mock.calls[0][0];
            expect(ctx.locals.authz.getRoles).toBeInstanceOf(Function);
            expect(ctx.locals.authz.getRoles())
                .toEqual({'user': ['tenant-1', 'tenant-2'], 'admin': ['tenant-2']});
            expect(ctx.locals.authz.getRoleNames).toBeInstanceOf(Function);
            expect(ctx.locals.authz.getRoleNames()).toEqual(['user', 'admin']);
            expect(ctx.locals.authz.getTenantIds).toBeInstanceOf(Function);
            expect(ctx.locals.authz.getTenantIds()).toEqual(['tenant-1', 'tenant-2']);
            expect(ctx.locals.authz.getTenantIds('user')).toEqual(['tenant-1', 'tenant-2']);
            expect(ctx.locals.authz.getTenantIds('admin')).toEqual(['tenant-2']);
            expect(ctx.locals.authz.hasRole).toBeInstanceOf(Function);
            expect(ctx.locals.authz.hasRole('user', 'tenant-1')).toBe(true);
            expect(ctx.locals.authz.hasRole('user', 'tenant-2')).toBe(true);
            expect(ctx.locals.authz.hasRole('user', ['tenant-1', 'tenant-2'])).toBe(true);
            expect(ctx.locals.authz.hasRole('user', 'tenant-3')).toBe(false);
            expect(ctx.locals.authz.hasRole('admin', 'tenant-2')).toBe(true);
            expect(ctx.locals.authz.hasRole('admin', 'tenant-1')).toBe(false);
            expect(ctx.locals.authz.hasRole('admin', ['tenant-1', 'tenant-2'])).toBe(false);
        });
    });

    describe('Authorization with application', () => {
        beforeEach(() => {
            actionHandler.mockClear();
            publicActionHandler.mockClear();
            privateActionHandler.mockClear();
            getRolesForApplicationHandler.mockClear();
        });

        it('should allow access to public actions', async () => {
            const broker = await createService({}, authnApp);
            const res = await broker.call(`${svcName}.publicAction`);
            expect(res).toBe('Result');
        });

        it('should allow access to private actions', async () => {
            const broker = await createService({}, authnApp);
            const res = await broker.call(`${svcName}.privateAction`);
            expect(res).toBe('Result');
        });

        it('should deny access to denied actions', async () => {
            const broker = await createService({}, authnApp);
            await expect(broker.call(`${svcName}.denyAction`))
                .rejects.toThrow('Forbidden');
        });

        it('should call `getRolesForApplication` action with the default parameter', async () => {
            const broker = await createService({}, authnApp);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(getRolesForApplicationHandler).toHaveBeenCalledTimes(1);
            expect(getRolesForApplicationHandler.mock.calls[0][0].params).toEqual({id: authnApp.id});
        });

        it('should call `getRolesForApplication` action with a custom parameter', async () => {
            const broker = await createService({ applicationRolesActionParam: 'custom' }, authnApp);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(getRolesForApplicationHandler).toHaveBeenCalledTimes(1);
            expect(getRolesForApplicationHandler.mock.calls[0][0].params).toEqual({custom: authnApp.id});
        });

        it('should call `getRolesForApplication` action with the secret parameter if set', async () => {
            const broker = await createService({ applicationRolesActionSecret: 'secret' }, authnApp);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(getRolesForApplicationHandler).toHaveBeenCalledTimes(1);
            expect(getRolesForApplicationHandler.mock.calls[0][0].params).toEqual({id: authnApp.id, secret: 'secret'});
        });

        it('should have application\'s roles', async () => {
            const broker = await createService({}, authnApp);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(actionHandler).toHaveBeenCalledTimes(1);

            const ctx = actionHandler.mock.calls[0][0];
            expect(ctx.locals.authz.getRoles).toBeInstanceOf(Function);
            expect(ctx.locals.authz.getRoles())
                .toEqual({'application': ['tenant-1']});
            expect(ctx.locals.authz.getRoleNames).toBeInstanceOf(Function);
            expect(ctx.locals.authz.getRoleNames()).toEqual(['application']);
            expect(ctx.locals.authz.getTenantIds).toBeInstanceOf(Function);
            expect(ctx.locals.authz.getTenantIds()).toEqual(['tenant-1']);
            expect(ctx.locals.authz.getTenantIds('application')).toEqual(['tenant-1']);
            expect(ctx.locals.authz.hasRole).toBeInstanceOf(Function);
            expect(ctx.locals.authz.hasRole('application', 'tenant-1')).toBe(true);
            expect(ctx.locals.authz.hasRole('application', 'tenant-2')).toBe(false);
        });
    });

    describe('Cache management', () => {
        beforeEach(() => {
            LRUCache.mockClear();
        });

        it('should clear the `entitiesRolesCache` on `applicationRolesChangedEvent` event without an appId', async () => {
            const broker = await createService();

            broker.emit('applicationRolesChangedEvent');

            expect(LRUCache.mock.instances[0].clear).toHaveBeenCalledTimes(1);
        });

        it('should remove a role from the `entitiesRolesCache` on `applicationRolesChangedEvent` event with an appId', async () => {
            const appId = 'role';
            const broker = await createService();

            broker.emit('applicationRolesChangedEvent', { appId });

            expect(LRUCache.mock.instances[0].delete).toHaveBeenCalledTimes(1);
            expect(LRUCache.mock.instances[0].delete).toHaveBeenCalledWith(`application:${appId}`);
        });

        it('should clear the `entitiesRolesCache` on `userRolesChangedEvent` event without a userId', async () => {
            const broker = await createService();

            broker.emit('userRolesChangedEvent');

            expect(LRUCache.mock.instances[0].clear).toHaveBeenCalledTimes(1);
        });

        it('should remove a role from the `entitiesRolesCache` on `userRolesChangedEvent` event with a userId', async () => {
            const userId = 'role';
            const broker = await createService();

            broker.emit('userRolesChangedEvent', { userId });

            expect(LRUCache.mock.instances[0].delete).toHaveBeenCalledTimes(1);
            expect(LRUCache.mock.instances[0].delete).toHaveBeenCalledWith(`user:${userId}`);
        });

        it('should remove the anonymous roles from the `entitiesRolesCache` on `anonymousRolesChangedEvent` event', async () => {
            const broker = await createService();

            broker.emit('anonymousRolesChangedEvent');

            expect(LRUCache.mock.instances[0].delete).toHaveBeenCalledTimes(1);
            expect(LRUCache.mock.instances[0].delete).toHaveBeenCalledWith('anonymous');
        });

        it('should clear the `roleAbilitiesCache` and `compiledAbilitiesCache` on `abilitiesChangedEvent` event without a roleName', async () => {
            const broker = await createService();

            broker.emit('abilitiesChangedEvent');

            expect(LRUCache.mock.instances[1].clear).toHaveBeenCalledTimes(1);
            expect(LRUCache.mock.instances[2].clear).toHaveBeenCalledTimes(1);
        });

        it('should remove a role from the `roleAbilitiesCache` and clear the `compiledAbilitiesCache` on `abilitiesChangedEvent` event with a roleName', async () => {
            const roleName = 'role';
            const broker = await createService();

            broker.emit('abilitiesChangedEvent', { roleName });

            expect(LRUCache.mock.instances[1].delete).toHaveBeenCalledTimes(1);
            expect(LRUCache.mock.instances[1].delete).toHaveBeenCalledWith(roleName);
            expect(LRUCache.mock.instances[2].clear).toHaveBeenCalledTimes(1);
        });
    });

    describe('Deprecated functionalities', () => {
        beforeEach(() => {
            actionHandler.mockClear();
            logger.warn.mockClear();
        });

        it('should call `ctx.locals.authz.getTenantIds()` when `getTenantIds()` is used', async () => {
            const mw = createMW({logger});
            expect(mw.methods.getTenantIds).toBeInstanceOf(Function);

            const ctx = {locals: {authz: {
                getTenantIds: jest.fn(() => ['tenant-1', 'tenant-2'])
            }}};

            expect(mw.methods.getTenantIds(ctx)).toEqual(['tenant-1', 'tenant-2']);
            expect(ctx.locals.authz.getTenantIds).toHaveBeenCalledTimes(1);
            expect(logger.warn).toHaveBeenCalledTimes(1);
            expect(logger.warn.mock.calls[0][0]).toMatch('`getTenantIds()` is deprecated');
        });

        it('should register the deprecated `setSubject()` method', async () => {
            const broker = await createService({}, authnUser);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(actionHandler).toHaveBeenCalledTimes(1);

            const ctx = actionHandler.mock.calls[0][0];
            expect(ctx.locals.setSubject).toBeInstanceOf(Function);
            const obj = ctx.locals.setSubject('MyType', {});
            expect(detectSubjectType(obj)).toBe('MyType');
            expect(logger.warn).toHaveBeenCalledTimes(1);
            expect(logger.warn.mock.calls[0][0]).toMatch('`ctx.locals.setSubject()` is deprecated');
        });

        it('should register the deprecated `abilities.can()` method', async () => {
            const broker = await createService({}, authnUser);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(actionHandler).toHaveBeenCalledTimes(1);

            const ctx = actionHandler.mock.calls[0][0];
            expect(ctx.locals.abilities.can).toBeInstanceOf(Function);
            expect(ctx.locals.abilities.can('action', svcName)).toBe(true);
            expect(ctx.locals.abilities.can('privateAction', svcName)).toBe(true);
            expect(ctx.locals.abilities.can('action', 'unknown')).toBe(false);
            expect(logger.warn).toHaveBeenCalledTimes(3);
            expect(logger.warn.mock.calls[0][0]).toMatch('`ctx.locals.abilities.can()` is deprecated');
        });

        it('should register the deprecated `abilities.rulesFor()` method', async () => {
            const broker = await createService({}, authnUser);
            const res = await broker.call(`${svcName}.action`);
            expect(res).toBe('Result');

            expect(actionHandler).toHaveBeenCalledTimes(1);

            const ctx = actionHandler.mock.calls[0][0];
            expect(ctx.locals.abilities.rulesFor).toBeInstanceOf(Function);
            expect(ctx.locals.abilities.rulesFor('action', svcName)).toMatchSnapshot();
            expect(logger.warn).toHaveBeenCalledTimes(1);
            expect(logger.warn.mock.calls[0][0]).toMatch('`ctx.locals.abilities.rulesFor()` is deprecated');
        });
    });
});
