const { generateKeyPairSync } = require('crypto');

const ServiceBroker = require('moleculer').ServiceBroker;

const { LRUCache } = require('lru-cache');
const TTLCache = require('@isaacs/ttlcache');
jest.mock('lru-cache');
jest.mock('@isaacs/ttlcache');

const jwt = require('jsonwebtoken');

const Middleware = require('../../../src/authentication.middleware');

const JWT_PARAMS_CACHE_KEY = 'jwtParams';

function generateKeyPair() {
    return generateKeyPairSync('ec', {
        namedCurve: 'prime256v1', // ES256
        publicKeyEncoding: {
            type: 'spki',
            format: 'pem'
        },
        privateKeyEncoding: {
            type: 'pkcs8',
            format: 'pem'
        }
    });
}

describe('Authentication middleware', () => {
    const appKeys = generateKeyPair();
    const jwtKeys = generateKeyPair();

    const jwtParams = {
        publicKey: jwtKeys.publicKey,
        issuer: 'my-service',
    };

    const appJwtParams = {
        issuer: 'my-application'
    };

    const userData = {
        email: 'user@example.com',
        ses: '32850aef-66a1-410c-9f0c-744deb3b52ff',
        sub: '86071c4f-9f7a-493b-9275-0250712e59e8'
    };

    const appData = {
        sub: 'd5a22505-19ef-4faa-b7ec-39993b77580f'
    };

    const validOpts = {
        userJWTParamsAction: 'my-service.getJWTParams',
        userJWTParamsChangeEvent: 'userJWTParamsChangeEvent',
        appAuthGetPublicKeyAction: 'my-service.getPublicKey',
        appAuthPublicKeyChangeEvent: 'appAuthPublicKeyChangeEvent',
        slugSeparator: '/',
    };

    function createBroker(opts) {
        const broker = new ServiceBroker({
            ...opts,
            nodeID: 'test',
            logger: false
        });

        return broker;
    }

    function createMW(opts = {}) {
        const broker = createBroker();
        const mw = Middleware(opts);
        mw.created(broker);
        return mw;
    }

    const actionHandler = jest.fn(() => Promise.resolve('Result'));
    const actionGetPublicKeyHandler = jest.fn((ctx) => {
        if (ctx.params.issuer === appJwtParams.issuer) {
            return Promise.resolve(appKeys.publicKey);
        }

        return Promise.reject(new Error('not found'));
    });
    const actionGetJWTParamsHandler = jest.fn(() => Promise.resolve(jwtParams));

    const serviceDefinition = {
        name: 'my-service',

        actions: {
            authenticationDisabled: {
                disableAuthentication: true,
                handler: actionHandler
            },

            action: {
                handler: actionHandler
            },

            getPublicKey: {
                handler: actionGetPublicKeyHandler
            },

            getJWTParams: {
                handler: actionGetJWTParamsHandler
            }
        }
    };

    async function createService(opts, internalServices = false) {
        const broker = createBroker({
            internalServices,
            middlewares: [Middleware(opts)]
        });

        broker.createService(serviceDefinition);

        await broker.start();

        return broker;
    }

    function signToken(userData, privateKey, options = {}) {
        return jwt.sign(
            userData,
            privateKey,
            {
                algorithm: 'ES256',
                issuer: jwtParams.issuer,
                ...options,
            }
        );
    }

    function signAppToken(appData, privateKey, options = {}) {
        return jwt.sign(
            appData,
            privateKey,
            {
                algorithm: 'ES256',
                issuer: appJwtParams.issuer,
                ...options,
            }
        );
    }

    describe('Middleware creation', () => {
        it('should create a middleware with default options', () => {
            expect(() => createMW()).not.toThrow();
        });

        it('should throw on creation when `userJWTParamsAction` parameter is invalid', () => {
            expect(() => createMW({
                userJWTParamsAction: null
            })).toThrow();
        });

        it('should throw on creation when `appAuthGetPublicKeyAction` parameter is invalid', () => {
            expect(() => createMW({
                appAuthGetPublicKeyAction: null
            })).toThrow();
        });

        it('should not throw on creation when all parameters are provided', () => {
            expect(() => createMW(validOpts)).not.toThrow();
        });

        it('should register hooks', () => {
            const mw = createMW(validOpts);

            expect(mw.created).toBeInstanceOf(Function);
            expect(mw.starting).toBeInstanceOf(Function);
            expect(mw.stopped).toBeInstanceOf(Function);
            expect(mw.localAction).toBeInstanceOf(Function);
        });
    });

    describe('Service lifecycle', () => {
        beforeEach(() => {
            TTLCache.mockClear();
            LRUCache.mockClear();
        });

        it('should initialize jwtParamsCache with default parameters', async () => {
            await createService(validOpts);

            expect(TTLCache).toHaveBeenCalledTimes(1);
            expect(TTLCache).toHaveBeenCalledWith({
                ttl: 60 * 60 * 1000 // 1 hour
            });
        });

        it('should initialize jwtParamsCache with custom parameters', async () => {
            await createService({
                ...validOpts,
                jwtParamsTTL: '5m'
            });

            expect(TTLCache).toHaveBeenCalledTimes(1);
            expect(TTLCache).toHaveBeenCalledWith({
                ttl: 5 * 60 * 1000 // 5 minutes
            });
        });

        it('should initialize applicationKeysCache with default parameters', async () => {
            await createService(validOpts);

            expect(LRUCache).toHaveBeenCalledTimes(1);
            expect(LRUCache).toHaveBeenCalledWith({
                max: 128,
                maxAge: 24 * 60 * 60 * 1000 // 1 day
            });
        });

        it('should initialize applicationKeysCache with custom parameters', async () => {
            await createService({
                ...validOpts,
                applicationKeysCacheSize: 32,
                applicationKeysCacheTTL: '1h'
            });

            expect(LRUCache).toHaveBeenCalledTimes(1);
            expect(LRUCache).toHaveBeenCalledWith({
                max: 32,
                maxAge: 60 * 60 * 1000 // 1 hour
            });
        });

        it('should not initialize caches for the $node internal service', async () => {
            await createService({
                ...validOpts,
                serviceBlacklist: []
            }, true);

            expect(TTLCache).toHaveBeenCalledTimes(1);
            expect(LRUCache).toHaveBeenCalledTimes(1);
        });

        it('should clear caches on broker stop', async () => {
            const broker = await createService(validOpts);

            expect(TTLCache).toHaveBeenCalledTimes(1);
            expect(LRUCache).toHaveBeenCalledTimes(1);
            expect(TTLCache.mock.instances[0].clear).not.toHaveBeenCalled();
            expect(LRUCache.mock.instances[0].clear).not.toHaveBeenCalled();

            await broker.stop();

            expect(TTLCache.mock.instances[0].clear).toHaveBeenCalledTimes(1);
            expect(LRUCache.mock.instances[0].clear).toHaveBeenCalledTimes(1);
        });
    });

    describe('Local action wrapper', () => {
        const broker = createBroker();
        const mw = createMW(validOpts);

        const handler = jest.fn(() => Promise.resolve('Result'));
        const action = {
            service: {
                name: 'my-service'
            },
            name: 'action',
            handler
        };

        afterAll(() => {
            delete action.disableAuthentication;
            action.service.name = 'my-service';
        });

        it('should wrap actions', async () => {
            const newHandler = mw.localAction.call(broker, handler, action);
            expect(newHandler).not.toBe(handler);
        });

        it('should not wrap actions on blacklisted services', async () => {
            action.service.name = '$node';
            const newHandler = mw.localAction.call(broker, handler, action);
            expect(newHandler).toBe(handler);
        });

        it('should not wrap actions that have the `disableAuthentication` flag', async () => {
            action.disableAuthentication = true;
            const newHandler = mw.localAction.call(broker, handler, action);
            expect(newHandler).toBe(handler);
        });
    });

    describe('Anonymous authentication (no token)', () => {
        beforeEach(() => {
            actionHandler.mockClear();
        });

        it('should allow anonymous access to actions', async () => {
            const broker = await createService(validOpts);

            const res = await broker.call('my-service.action');
            expect(actionHandler).toHaveBeenCalledTimes(1);
            expect(res).toBe('Result');
        });

        it('should populate the context with authentication information', async () => {
            const broker = await createService(validOpts);

            await broker.call('my-service.action');

            expect(actionHandler).toHaveBeenCalledTimes(1);
            expect(actionHandler.mock.calls[0][0].locals).toBeDefined();
            expect(actionHandler.mock.calls[0][0].locals.authn).toEqual({
                kind: 'anonymous',
                slug: 'anonymous'
            });
        });

        it('should not populate the context when `disableAuthentication` is set', async () => {
            const broker = await createService(validOpts);

            await broker.call('my-service.authenticationDisabled');

            expect(actionHandler).toHaveBeenCalledTimes(1);
            expect(actionHandler.mock.calls[0][0].locals).toBeDefined();
            expect(actionHandler.mock.calls[0][0].locals.authn).toBeUndefined();
        });

        it('should set `ctx.locals.authn` and all properties read-only', async () => {
            const broker = await createService(validOpts, true);

            await broker.call('my-service.action');

            const ctx = actionHandler.mock.calls[0][0];
            expect(Object.isFrozen(ctx.locals.authn)).toBe(true);
            expect(Object.getOwnPropertyDescriptor(ctx.locals.authn, 'kind').writable).toBe(false);
            expect(Object.getOwnPropertyDescriptor(ctx.locals.authn, 'slug').writable).toBe(false);
        });
    });

    describe('User authentication (User token)', () => {
        const userToken = signToken(userData, jwtKeys.privateKey);

        beforeEach(() => {
            TTLCache.mockClear();
            actionHandler.mockClear();
            actionGetJWTParamsHandler.mockClear();
        });

        it('should fetch jwtParams from the `userJWTParamsAction` action', async () => {
            const broker = await createService(validOpts, true);

            await broker.call('my-service.action', {}, { meta: { token: userToken } });

            expect(actionGetJWTParamsHandler).toHaveBeenCalledTimes(1);

            expect(TTLCache.mock.instances[0].set).toHaveBeenCalledWith(
                JWT_PARAMS_CACHE_KEY,
                jwtParams
            );
        });

        it('should fetch jwtParams from the cache', async () => {
            const broker = await createService(validOpts, true);

            await broker.call('my-service.action', {}, { meta: { token: userToken } });
            expect(TTLCache.mock.instances[0].has).toHaveBeenCalledWith(JWT_PARAMS_CACHE_KEY);
            expect(TTLCache.mock.instances[0].get).not.toHaveBeenCalled();
            expect(TTLCache.mock.instances[0].set).toHaveBeenCalledTimes(1);

            TTLCache.mock.instances[0].has = jest.fn(() => true);
            TTLCache.mock.instances[0].get = jest.fn(() => jwtParams);
            TTLCache.mock.instances[0].set.mockClear();

            await broker.call('my-service.action', {}, { meta: { token: userToken } });
            expect(TTLCache.mock.instances[0].has).toHaveBeenCalledWith(JWT_PARAMS_CACHE_KEY);
            expect(TTLCache.mock.instances[0].get).toHaveBeenCalledWith(JWT_PARAMS_CACHE_KEY);
            expect(TTLCache.mock.instances[0].set).not.toHaveBeenCalled();
        });

        it('should throw when `jwt` settings are invalid', async () => {
            serviceDefinition.actions.getJWTParams.handler = jest.fn(() => Promise.resolve({}));
            const broker = await createService(validOpts, true);

            await expect(broker.call('my-service.action', {}, { meta: { token: userToken } }))
                .rejects.toThrow();

            serviceDefinition.actions.getJWTParams.handler = actionGetJWTParamsHandler;
        });

        it('should throw when the token is expired', async () => {
            const token = signToken(
                userData,
                jwtKeys.privateKey,
                { expiresIn: -60 } // 1 minute ago
            );

            const broker = await createService(validOpts, true);

            await expect(broker.call('my-service.action', {}, { meta: { token } }))
                .rejects.toThrow();
        });

        it('should throw when the token is invalid', async () => {
            const broker = await createService(validOpts, true);

            await expect(broker.call('my-service.action', {}, { meta: { token: 'invalid' } }))
                .rejects.toThrow();
        });

        it('should throw when the token is not signed with the correct key', async () => {
            const token = signToken(userData, appKeys.privateKey);
            const broker = await createService(validOpts, true);

            await expect(broker.call('my-service.action', {}, { meta: { token } }))
                .rejects.toThrow();
        });

        it('should throw when the token is not issued by the correct issuer', async () => {
            const token = signToken(userData, jwtKeys.privateKey, { issuer: 'other-service' });
            const broker = await createService(validOpts, true);

            await expect(broker.call('my-service.action', {}, { meta: { token } }))
                .rejects.toThrow();
        });

        it('should honor clock `jwtClockTolerance` settings', async () => {
            const token = signToken(
                userData,
                jwtKeys.privateKey,
                { expiresIn: -45 } // 45 seconds ago
            );

            const broker = await createService({
                ...validOpts,
                jwtClockTolerance: 60 // 1 minute
            }, true);

            await broker.call('my-service.action', {}, { meta: { token } });
            expect(actionHandler).toHaveBeenCalledTimes(1);
        });

        it('should allow access to actions with a valid token', async () => {
            const broker = await createService(validOpts, true);

            const res = await broker.call('my-service.action', {}, { meta: { token: userToken } });
            expect(actionHandler).toHaveBeenCalledTimes(1);
            expect(res).toBe('Result');
        });

        it('should populate the context with user information', async () => {
            const broker = await createService(validOpts, true);

            await broker.call('my-service.action', {}, { meta: { token: userToken } });

            expect(actionHandler).toHaveBeenCalledTimes(1);
            expect(actionHandler.mock.calls[0][0].locals).toBeDefined();
            expect(actionHandler.mock.calls[0][0].locals.authn).toEqual({
                kind: 'user',
                id: userData.sub,
                email: userData.email,
                sessid: userData.ses,
                slug: `user${validOpts.slugSeparator}${userData.sub}${validOpts.slugSeparator}${userData.ses}`
            });
        });

        it('should set `ctx.locals.authn` and all properties read-only', async () => {
            const broker = await createService(validOpts, true);

            await broker.call('my-service.action', {}, { meta: { token: userToken } });

            const ctx = actionHandler.mock.calls[0][0];
            expect(Object.isFrozen(ctx.locals.authn)).toBe(true);
            expect(Object.getOwnPropertyDescriptor(ctx.locals.authn, 'kind').writable).toBe(false);
            expect(Object.getOwnPropertyDescriptor(ctx.locals.authn, 'id').writable).toBe(false);
            expect(Object.getOwnPropertyDescriptor(ctx.locals.authn, 'email').writable).toBe(false);
            expect(Object.getOwnPropertyDescriptor(ctx.locals.authn, 'sessid').writable).toBe(false);
            expect(Object.getOwnPropertyDescriptor(ctx.locals.authn, 'slug').writable).toBe(false);
        });
    });

    describe('Application authentication (Application token)', () => {
        beforeEach(() => {
            LRUCache.mockClear();
            actionHandler.mockClear();
            actionGetPublicKeyHandler.mockClear();
        });

        it('should fetch the application public key from the `appAuthGetPublicKeyAction` action', async () => {
            const appToken = signAppToken(appData, appKeys.privateKey);
            const broker = await createService(validOpts, true);

            await broker.call('my-service.action', {}, { meta: { appToken } });

            expect(actionGetPublicKeyHandler).toHaveBeenCalledTimes(1);
            expect(actionGetPublicKeyHandler.mock.calls[0][0].params).toEqual({ issuer: appJwtParams.issuer });
        });

        it('should fetch the application public key from the cache', async () => {
            const appToken = signAppToken(appData, appKeys.privateKey);
            const broker = await createService(validOpts, true);

            await broker.call('my-service.action', {}, { meta: { appToken } });

            expect(LRUCache.mock.instances[0].has).toHaveBeenCalledWith(appJwtParams.issuer);
            expect(LRUCache.mock.instances[0].get).not.toHaveBeenCalled();
            expect(LRUCache.mock.instances[0].set).toHaveBeenCalledTimes(1);

            LRUCache.mock.instances[0].has = jest.fn(() => true);
            LRUCache.mock.instances[0].get = jest.fn(() => appKeys.publicKey);
            LRUCache.mock.instances[0].set.mockClear();
            actionGetPublicKeyHandler.mockClear();

            await broker.call('my-service.action', {}, { meta: { appToken } });

            expect(LRUCache.mock.instances[0].has).toHaveBeenCalledWith(appJwtParams.issuer);
            expect(LRUCache.mock.instances[0].get).toHaveBeenCalledWith(appJwtParams.issuer);
            expect(LRUCache.mock.instances[0].set).not.toHaveBeenCalled();
            expect(actionGetPublicKeyHandler).not.toHaveBeenCalled();
        });

        it('should throw when the application public key is missing', async () => {
            const appToken = signAppToken(appData, appKeys.privateKey, { issuer: 'invalid' });
            const broker = await createService(validOpts, true);

            await expect(broker.call('my-service.action', {}, { meta: { appToken } }))
                .rejects.toThrow();

            expect(actionGetPublicKeyHandler).toHaveBeenCalledTimes(1);
            expect(actionGetPublicKeyHandler.mock.calls[0][0].params).toEqual({ issuer: 'invalid' });
        });

        it('should throw when the token is expired', async () => {
            const appToken = signAppToken(
                appData,
                appKeys.privateKey,
                { expiresIn: -60 } // 1 minute ago
            );

            const broker = await createService(validOpts, true);

            await expect(broker.call('my-service.action', {}, { meta: { appToken } }))
                .rejects.toThrow();
        });

        it('should throw when the token is invalid', async () => {
            const broker = await createService(validOpts, true);

            await expect(broker.call('my-service.action', {}, { meta: { appToken: 'invalid' } }))
                .rejects.toThrow();
        });

        it('should throw when the token is not signed with the correct key', async () => {
            const appToken = signAppToken(userData, jwtKeys.privateKey);

            const broker = await createService(validOpts, true);
            await expect(broker.call('my-service.action', {}, { meta: { appToken } }))
                .rejects.toThrow();
        });

        it('should throw when the token is valid but too old', async () => {
            const appToken = signAppToken(
                {
                    ...appData,
                    iat: Math.floor(Date.now() / 1000) - (60 * 60) // 1 hour ago
                },
                appKeys.privateKey,
                { expiresIn: 24 * 60 * 60 } // 24 hours
            );

            const broker = await createService(validOpts, true);
            await expect(broker.call('my-service.action', {}, { meta: { appToken } }))
                .rejects.toThrow();
        });

        it('should populate the context with application information', async () => {
            const appToken = signAppToken(appData, appKeys.privateKey);
            const broker = await createService(validOpts, true);

            await broker.call('my-service.action', {}, { meta: { appToken } });

            expect(actionHandler).toHaveBeenCalledTimes(1);
            expect(actionHandler.mock.calls[0][0].locals).toBeDefined();
            expect(actionHandler.mock.calls[0][0].locals.authn).toEqual({
                kind: 'application',
                id: appData.sub,
                slug: `application${validOpts.slugSeparator}${appData.sub}`
            });
        });

        it('should allow access to actions with a valid token', async () => {
            const appToken = signAppToken(appData, appKeys.privateKey);
            const broker = await createService(validOpts, true);

            const res = await broker.call('my-service.action', {}, { meta: { appToken } });
            expect(actionHandler).toHaveBeenCalledTimes(1);
            expect(res).toBe('Result');
        });

        it('should set `ctx.locals.authn` and all properties read-only', async () => {
            const appToken = signAppToken(appData, appKeys.privateKey);
            const broker = await createService(validOpts, true);

            await broker.call('my-service.action', {}, { meta: { appToken } });

            const ctx = actionHandler.mock.calls[0][0];
            expect(Object.isFrozen(ctx.locals.authn)).toBe(true);
            expect(Object.getOwnPropertyDescriptor(ctx.locals.authn, 'kind').writable).toBe(false);
            expect(Object.getOwnPropertyDescriptor(ctx.locals.authn, 'id').writable).toBe(false);
            expect(Object.getOwnPropertyDescriptor(ctx.locals.authn, 'slug').writable).toBe(false);
        });
    });

    describe('Cache management', () => {
        beforeEach(() => {
            TTLCache.mockClear();
            LRUCache.mockClear();
        });

        it('should clear the jwtParams cache on `userJWTParamsChangeEvent` event', async () => {
            const broker = await createService(validOpts, true);

            await broker.emit('userJWTParamsChangeEvent');

            expect(TTLCache.mock.instances[0].clear).toHaveBeenCalledTimes(1);
        });

        it('should clear the applicationKeys cache on `appAuthPublicKeyChangeEvent` event', async () => {
            const broker = await createService(validOpts, true);

            await broker.emit('appAuthPublicKeyChangeEvent');

            expect(LRUCache.mock.instances[0].clear).toHaveBeenCalledTimes(1);
        });

        it('should remove an application key from the cache on `appAuthPublicKeyChangeEvent` event with an application name', async () => {
            const appName = 'myApp';
            const broker = await createService(validOpts, true);

            await broker.emit('appAuthPublicKeyChangeEvent', { appName });

            expect(LRUCache.mock.instances[0].delete).toHaveBeenCalledTimes(1);
            expect(LRUCache.mock.instances[0].delete).toHaveBeenCalledWith(appName);
        });
    });

    describe('Deprecated functionalities', () => {
        beforeEach(() => {
            actionHandler.mockClear();
        });

        it('should populate `ctx.locals.token` with the token', async () => {
            const token = signToken(userData, jwtKeys.privateKey);
            const decodedToken = jwt.decode(token);
            const broker = await createService(validOpts, true);

            await broker.call('my-service.action', {}, { meta: { token } });

            expect(actionHandler).toHaveBeenCalledTimes(1);
            expect(actionHandler.mock.calls[0][0].locals).toBeDefined();
            expect(actionHandler.mock.calls[0][0].locals.token).toEqual(decodedToken);
        });
    });
});
