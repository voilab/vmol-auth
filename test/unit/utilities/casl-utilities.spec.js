const { PureAbility, subject } = require('@casl/ability');

const { unpackRules, conditionsMatcher, detectSubjectType } = require('../../../lib/casl-utilities');

describe('CASL Utilities Tests', () => {
    const user_id = '657538014677958657';

    function buildCtx(service, role = 'admin', opts = {}) {
        const tenant_id = 'valid-1';

        const ctx = {
            service: {
                name: service
            },
            locals: {
                token: {
                    roles: {}
                }
            },
            params: {
                tenant_id,
                email: 'another@email.com'
            },
            ...opts
        };

        ctx.locals.token.roles[role] = [
            'valid-1',
            'valid-2',
            'valid-3'
        ];

        ctx.locals.authn = {
            kind: 'user',
            id: user_id,
            email: 'user@example.com',
            sessid: '945451993715245059',
            slug: 'user/657538014677958657/945451993715245059'
        };

        ctx.locals.authz = {
            hasRole(role, tenant_id) {
                if ((typeof tenant_id !== 'string' && !Array.isArray(tenant_id)) || !tenant_id.length) {
                    throw new Error(`tenant_id is not a valid string or array, got ${tenant_id}`);
                }

                if (Array.isArray(tenant_id)) {
                    // Check if all tenant ids are present in the token
                    return tenant_id.every(id => ctx.locals.token.roles[role].includes(id));
                }

                return ctx.locals.token.roles[role].includes(tenant_id);
            }
        };

        return ctx;
    }

    const packedAbilities = [
        [ 'subj', 'hasRole', { $hasRole: ['team', 'params.tenant_id'] } ],
        [ 'subj', 'hasRoleMulti', { $hasRole: ['team', 'params.multiple_ids'] } ],
        [ 'subj', 'notHasRole', { $notHasRole: ['team', 'params.tenant_id'] } ],
        [ 'subj', 'hasRoleWrongField', { $hasRole: ['team', 'params.doesntexists'] } ],
        [ 'subj', 'hasRoleDefault', { $hasRole: 'team' } ],
        [ 'subj', 'interpolate', {
            $hasRole: ['admin', 'params.tenant_id'],
            $interpolate: { 'params.email': { $ne: 'locals.authn.email' } }
        } ],
        [ 'subj', 'userid', {
            $userIdMatches: 'params.user_id'
        } ]
    ];

    const abilities = new PureAbility(
        unpackRules(packedAbilities),
        {
            detectSubjectType,
            conditionsMatcher
        }
    );

    function buildRule(conditions) {
        return { subject: 'sub', action: 'act', conditions };
    }

    function testRule(condition) {
        const ab = new PureAbility([buildRule(condition)], { detectSubjectType, conditionsMatcher });
        ab.can('act', buildCtx('sub'));
    }

    describe('Subject type detection', () => {
        it('should be compatible with casl\'s subject()', () => {
            const obj = {};
            subject('MyType', obj);
            expect(detectSubjectType(obj)).toBe('MyType');
        });

        it('should use Moleculer\'s service name as subject type', () => {
            const ctx = buildCtx('MyService');
            expect(detectSubjectType(ctx)).toBe('MyService');
        });

        it('should use the Constructor name as subject type', () => {
            const obj = new class MyClass {};
            expect(detectSubjectType(obj)).toBe('MyClass');
        });
    });

    describe('Abilities unpacker', () => {
        it('should unpack abilities', () => {
            const testAbilities = [
                [ 'subject1', 'action1', { 'field': 1 } ],
                [ 'subject2', 'action2' ]
            ];
            const ab = unpackRules(testAbilities);

            expect(ab).toHaveLength(testAbilities.length);
            expect(ab[0].subject).toBe('subject1');
            expect(ab[0].action).toBe('action1');
            expect(ab[0].conditions).toEqual({ 'field': 1 });
            expect(ab[1].subject).toBe('subject2');
            expect(ab[1].action).toBe('action2');
            expect(ab[1].conditions).toBeUndefined();
        });

        it('should handle already unpacked abilities gracefully', () => {
            const testAbilities = [
                { subject: 'subject1', action: 'action1', conditions: { 'field': 1 } },
                { subject: 'subject2', action: 'action2' }
            ];
            const ab = unpackRules(testAbilities);

            expect(ab).toHaveLength(testAbilities.length);
            expect(ab[0].subject).toBe('subject1');
            expect(ab[0].action).toBe('action1');
            expect(ab[0].conditions).toEqual({ 'field': 1 });
            expect(ab[1].subject).toBe('subject2');
            expect(ab[1].action).toBe('action2');
            expect(ab[1].conditions).toBeUndefined();
        });

        it('should throw on invalid input', () => {
            expect(() => unpackRules('string')).toThrow();
            expect(() => unpackRules([ 'subject', 'action', {} ])).toThrow();
        });

        it('should throw on invalid rule', () => {
            expect(() => unpackRules([[ 'subject' ]])).toThrow();
            expect(() => unpackRules([[ 'subject', 1 ]])).toThrow();
            expect(() => unpackRules([[ 1, 'action' ]])).toThrow();
            expect(() => unpackRules([[ 1, 1 ]])).toThrow();
            expect(() => unpackRules([[ 'subject', 'action', 'conditions' ]])).toThrow();
            expect(() => unpackRules([[ 'subject', 'action', {}, 'extra' ]])).toThrow();
        });
    });

    describe('$hasRole and $notHasRole operators', () => {
        it('should throw on invalid $hasRole parameters', () => {
            expect(() => testRule({ $hasRole: 1234 })).toThrow();
            expect(() => testRule({ $hasRole: null })).toThrow();
            expect(() => testRule({ $hasRole: 'admin' })).not.toThrow();
            expect(() => testRule({ $hasRole: ['admin'] })).toThrow();
            expect(() => testRule({ $hasRole: ['admin', 'params.tenant_id'] })).not.toThrow();
            expect(() => testRule({ $hasRole: [1234, 'params.tenant_id'] })).toThrow();
            expect(() => testRule({ $hasRole: ['admin', 1234] })).toThrow();
            expect(() => testRule({ $hasRole: ['admin', null] })).toThrow();
            expect(() => testRule({ $hasRole: ['admin', 'params.tenant_id', 'field2'] })).toThrow();
        });

        it('should throw when authz.hasRole is not a function', () => {
            const ctx = buildCtx('subj');
            ctx.locals.authz.hasRole = null;
            expect(() => abilities.can('hasRole', ctx)).toThrow();
        });

        it('should throw when the specified field is null or undefined', () => {
            expect(() => abilities.can('hasRoleWrongField', buildCtx('subj', 'team'))).toThrow();
        });

        it('should pass with $hasRole', () => {
            expect(abilities.can('hasRole', buildCtx('subj', 'team'))).toBe(true);
        });

        it('should pass with $hasRole and multiple tenant ids', () => {
            expect(abilities.can('hasRoleMulti', buildCtx('subj', 'team', { params: { multiple_ids: ['valid-1', 'valid-2'] } }))).toBe(true);
        });

        it('should fail with $hasRole and multiple tenant ids and one rong id', () => {
            expect(abilities.can('hasRoleMulti', buildCtx('subj', 'team', { params: { multiple_ids: ['valid-1', 'invalid'] } }))).toBe(false);
        });

        it('should fail with $hasRole and wrong tenant', () => {
            expect(abilities.can('hasRole', buildCtx('subj', 'team', { params: { tenant_id: 'invalid' } }))).toBe(false);
        });

        it('should pass with $hasRole and default field name', () => {
            expect(abilities.can('hasRoleDefault', buildCtx('subj', 'team'))).toBe(true);
        });

        it('should pass with $notHasRole', () => {
            expect(abilities.can('notHasRole', buildCtx('subj', 'team', { params: { tenant_id: 'invalid' } }))).toBe(true);
        });
    });

    describe('$userIdMatches operator', () => {
        it('should throw on invalid $userIdMatches parameters', () => {
            expect(() => testRule({ $userIdMatches: 1234 })).toThrow();
            expect(() => testRule({ $userIdMatches: null })).toThrow();
            expect(() => testRule({ $userIdMatches: 'user_id' })).not.toThrow();
            expect(() => testRule({ $userIdMatches: 'params.user_id' })).not.toThrow();
        });

        it('should pass with $userIdMatches', () => {
            expect(abilities.can('userid', buildCtx('subj', 'team', { params: { user_id } }))).toBe(true);
        });

        it('should fails with $userIdMatches and wrong user_id', () => {
            expect(abilities.can('userid', buildCtx('subj', 'team', { params: { user_id: 'wrong' } }))).toBe(false);
        });
    });

    describe('$interpolate operator', () => {
        it('should throw on invalid $interpolate parameters', () => {
            expect(() => testRule({ $interpolate: 1234 })).toThrow();
            expect(() => testRule({ $interpolate: null })).toThrow();
            expect(() => testRule({ $interpolate: {} })).toThrow();
            expect(() => testRule({ $interpolate: { 'params.email': { $ne: 'locals.authn.email' } } })).not.toThrow();
        });

        it('should interpolate values', () => {
            expect(abilities.can('interpolate', buildCtx('subj', 'admin'))).toBe(true);
        });
    });
});
