const { buildMongoQueryMatcher } = require('@casl/ability');

/**
 * Unpacks rules returned from the ability service
 *
 * @param {Array} rules - Array of rules in the form of [subject, action, conditions]
 * @returns {Array} - Array of rules in the form of {subject, action, conditions}
 */
const unpackRules = (rules) => {
    if (!Array.isArray(rules)) {
        throw new TypeError('unpackRules expects to receive an array of rules');
    }

    return rules.map(
        (rule) => {
            // Rule is already unpacked
            if (!Array.isArray(rule) && typeof rule.subject === 'string' && typeof rule.action === 'string') {
                return rule;
            }

            if (!Array.isArray(rule) || rule.length < 2 || rule.length > 3) {
                throw new TypeError(`unpackRules expects to receive an array of rules in the form of [subject, action, conditions], got ${typeof rule} with ${rule}`);
            }

            if (typeof rule[0] !== 'string' || typeof rule[1] !== 'string') {
                throw new TypeError(`unpackRules expects subject and action to be strings, got ${rule}`);
            }

            const caslRule = {
                subject: rule[0],
                action: rule[1]
            };

            if (rule[2] !== undefined && typeof rule[2] !== 'object') {
                throw new TypeError(`unpackRules expects conditions to be an object, got ${rule[2]}`);
            }

            if (typeof rule[2] === 'object') {
                caslRule.conditions = rule[2];
            }

            return caslRule;
        }
    );
};

/**
 * Detects the subject type from a moleculer context object
 */
function detectSubjectType(obj) {
    if (typeof obj?.service?.name === 'string') {
        return obj.service.name;
    }

    if (obj.__caslSubjectType__ !== undefined) {
        return obj.__caslSubjectType__;
    }

    return obj.constructor.name;
}

/**
 * Parses the role conditions and validates the input
 * The role condition can be either a string containing the role name
 * or an array containing the role name and the id_field
 * The default id_field is 'params.tenant_id'
 *
 * @param {Object} instruction - The instruction object
 * @param {Array|String} value - The value to be parsed
 * @returns {Array} - Array of role and id_field
 */
function parseRole(instruction, value) {
    if (!Array.isArray(value) && typeof value !== 'string') {
        throw new TypeError(`$${instruction.name} expects to receive an array or a string, but received ${value}`);
    }

    if (Array.isArray(value) && value.length !== 2) {
        throw new TypeError(`$${instruction.name} expects to receive an array with 2 elements, but received ${value}`);
    }

    let role, id_field;

    if (Array.isArray(value)) {
        [role, id_field] = value;
    } else {
        role = value;
        id_field = 'params.tenant_id';
    }

    if (typeof role !== 'string') {
        throw new TypeError(`$${instruction.name} expects first argument to be a string representing a role, but received ${role}`);
    }

    if (typeof id_field !== 'string') {
        throw new TypeError(`$${instruction.name} expects second argument to be a string representing a id_field, but received ${id_field}`);
    }

    return [role, id_field];
}

const $hasRole = {
    type: 'document',

    validate(instruction, value) {
        parseRole(instruction, value);
    }
};

/**
 * The $hasRole operator calls the hasRole() method on the context's authz object
 * It retreives the tenant_id from the context using the field specified in the condition
 */
const hasRole = (condition, ctx, { get }) => {
    const [role, id_field] = parseRole(condition, condition.value);

    if (typeof ctx?.locals?.authz.hasRole !== 'function') {
        throw new Error(`$${condition.operator}: ctx.locals.authz.hasRole is not a function`);
    }

    const tenant_id = get(ctx, id_field);

    if (tenant_id === null || tenant_id === undefined) {
        throw new Error(`$${condition.operator}: ctx.${id_field} is null or undefined`);
    }

    return ctx.locals.authz.hasRole(role, tenant_id);
};

/**
 * The $notHasRole operator calls the $hasRole operator and negates the result
 * It takes the same arguments as the $hasRole operator
 */
const $notHasRole = $hasRole;

const notHasRole = (...args) => {
    return !hasRole(...args);
};

/**
 * The $userIdMatches operator is used to compare the user_id from the session with a field in the context
 * It takes a single argument which is the path to the field in the context
 */
const $userIdMatches = {
    type: 'document',

    validate(instruction, value) {
        if (typeof value !== 'string') {
            throw new Error(`$${instruction.name}: expects to receive a string`);
        }
    }
};

const userIdMatches = (condition, ctx, { get }) => {
    const user_id = get(ctx, condition.value);

    if (!user_id) {
        return false;
    }

    return ctx.locals.authn.id === user_id;
};

/**
 * The $interpolate operator is used to interpolate values from the context into the conditions
 * It can be compounded with any other field operator
 */
const $interpolate = {
    type: 'compound',

    validate(instruction, value) {
        if (!value || value.constructor !== Object) {
            throw new Error(`$${instruction.name}: expects to receive an object with nested query or field level operators`);
        }
    }
};

const interpolate = (node, object, { interpret, get }) => {
    node.value.forEach(condition => {
        condition.value = get(object, condition.value);
    });

    return node.value.some(cond => interpret(cond, object));
};

/**
 * conditionsMatcher including the custom operators
 * It need to be passed to PureAbility on creation
 */
const conditionsMatcher = buildMongoQueryMatcher(
    { $hasRole, $notHasRole, $interpolate, $userIdMatches },
    { hasRole, notHasRole, interpolate, userIdMatches }
);

module.exports = {
    unpackRules,
    conditionsMatcher,
    detectSubjectType
};
