'use strict';

const ms = require('ms');
const { LRUCache } = require('lru-cache');
const { MoleculerClientError, ServiceSchemaError, MoleculerServerError } = require('moleculer').Errors;
const { PureAbility, subject } = require('@casl/ability');

const { unpackRules, conditionsMatcher, detectSubjectType } = require('../lib/casl-utilities');

const DEFAULT_ROLE_CACHE_KEY = ':default:';
const CACHE_KEY_APPLICATION = 'application';
const CACHE_KEY_USER = 'user';
const CACHE_KEY_ANONYMOUS = 'anonymous';

/**
 * @summary Middleware for Moleculer services to handle authorization.
 * @description This middleware is used to handle authorization in Moleculer services. It uses the `@casl/ability` package to define and check abilities.
 *
 * @param {Object} opts - The options for the middleware.
 * @param {String} opts.abilitiesForRoleAction - Name of the action to call to get the abilities for a role.
 * @param {String} opts.defaultAbilitiesAction - Name of the action to call to get the default abilities.
 * @param {String} opts.abilitiesChangedEvent - Name of the event to listen for to clear the abilities cache.
 * @param {String} opts.applicationRolesAction - Name of the action to call to get the roles for an application.
 * @param {String} opts.applicationRolesActionParam - Name of the parameter containing the application ID.
 * @param {String} opts.applicationRolesActionSecret - Optional secret to send with the request.
 * @param {String} opts.applicationRolesChangedEvent - Name of the event to listen for to clear the application roles cache.
 * @param {String} opts.userRolesAction - Name of the action to call to get the roles for a user.
 * @param {String} opts.userRolesActionParam - Name of the parameter containing the user ID.
 * @param {String} opts.userRolesActionSecret - Optional secret to send with the request.
 * @param {String} opts.userRolesChangedEvent - Name of the event to listen for to clear the user roles cache.
 * @param {String} opts.anonymousRolesAction - Name of the action to call to get the anonymous roles. Can be null.
 * @param {String} opts.anonymousRolesChangedEvent - Name of the event to listen for to clear the anonymous roles cache.
 * @param {Number} opts.entitiesRolesCacheMax - Maximum number of entities to cache roles for.
 * @param {String} opts.entitiesRolesCacheTTL - Time-to-live for the entities roles cache.
 * @param {Number} opts.roleAbilitiesCacheMax - Maximum number of roles to cache abilities for.
 * @param {String} opts.roleAbilitiesCacheTTL - Time-to-live for the role abilities cache.
 * @param {Number} opts.compiledAbilitiesCacheMax - Maximum number of role combinations to cache compiled abilities for.
 * @param {String} opts.compiledAbilitiesCacheTTL - Time-to-live for the compiled abilities cache.
 * @param {String} opts.eventsServiceName - Name of the service to listening for events.
 * @param {Array} opts.serviceBlacklist - List of service names to blacklist from the middleware.
 */
module.exports = function AuthenticationMiddleware(opts) {
    opts = Object.assign({}, {
        abilitiesForRoleAction: 'authorization.getAbilitiesForRole',
        defaultAbilitiesAction: 'authorization.getDefaultAbilities',
        abilitiesChangedEvent: null,

        applicationRolesAction: 'authorization.getRolesForApplication',
        applicationRolesActionParam: 'id',
        applicationRolesActionSecret: null,
        applicationRolesChangedEvent: null,

        userRolesAction: 'authorization.getRolesForUser',
        userRolesActionParam: 'id',
        userRolesActionSecret: null,
        userRolesChangedEvent: null,

        anonymousRolesAction: 'authorization.getRolesForAnonymous',
        anonymousRolesChangedEvent: null,

        entitiesRolesCacheMax: 8192,
        entitiesRolesCacheTTL: '4h',

        roleAbilitiesCacheMax: 8,
        roleAbilitiesCacheTTL: '1d',

        compiledAbilitiesCacheMax: 32,
        compiledAbilitiesCacheTTL: '1d',

        eventsServiceName: 'AuthorizationEvents',

        serviceBlacklist: [],
    }, opts);

    opts.serviceBlacklist.push('$node');

    /**
     * @type {LRUCache} - Cache for applications, users and default roles. One entry per entity containing only the role names.
     */
    let entitiesRolesCache = null;

    /**
     * @type {LRUCache} - Cache for uncompiled role abilities. One entry per role containing all the raw abilities for that role.
     */
    let roleAbilitiesCache = null;

    /**
     * @type {LRUCache} - Cache for compiled multi-role abilities. One entry per role combination (eg: admin + user) containing the compiled abilities for that combination.
     */
    let compiledAbilitiesCache = null;

    /**
     * @type {Object} - The logger for the middleware.
     */
    let logger = null;

    /**
     * Asserts that the current session has the ability to call the given action.
     *
     * @param {Object} ctx - The context object for the action.
     * @param {Object} action - The action object.
     * @returns {Boolean} - Returns true if the session has the ability to call the action.
     * @throws {MoleculerClientError} - Throws a MoleculerClientError if the session does not have the ability to call the action.
     */
    function assertAbilityToCallAction(ctx, action) {
        const isAuthorized = ctx.locals.authz.can(action.rawName, ctx);

        if (!isAuthorized) {
            const message = `Cannot execute "${action.rawName}" on "${detectSubjectType(ctx)}"`;

            if (ctx.locals.authn.kind === 'anonymous') {
                throw new MoleculerClientError('Unauthorized', 401, 'ERR_UNAUTHORIZED', { message });
            }

            throw new MoleculerClientError('Forbidden', 403, 'ERR_FORBIDDEN', { message });
        }

        return isAuthorized;
    }

    /**
     * Fetches the abilities for a role from the cache or the service.
     * If the role is not specified, the default abilities are fetched.
     *
     * @param {Object} ctx - The context object for the action.
     * @param {String} role - The name of the role to fetch the abilities for.
     * @returns {Promise} - Returns a promise that resolves with the abilities for the role.
     */
    async function fetchAbilitiesForRole(ctx, role = null) {
        const cacheKey = role || DEFAULT_ROLE_CACHE_KEY;

        let abilities = roleAbilitiesCache.get(cacheKey);

        if (!abilities) {
            if (typeof role === 'string' && role) {
                abilities = await ctx.broker.call(opts.abilitiesForRoleAction, { role });
            } else {
                abilities = await ctx.broker.call(opts.defaultAbilitiesAction);
            }
            roleAbilitiesCache.set(cacheKey, Array.isArray(abilities) ? abilities : []);
        }

        return abilities;
    }

    /**
     * Fetches the compiled abilities for a set of roles from the cache or the service.
     * If no roles are specified, the default abilities are fetched.
     *
     * @param {Object} ctx - The context object for the action.
     * @param {Array} roles - The names of the roles to fetch the abilities for.
     * @returns {Promise} - Returns a promise that resolves with the compiled abilities for the roles.
     */
    async function getAbilitiesForRoles(ctx, roles = []) {
        let cacheKey = roles.join(':') || DEFAULT_ROLE_CACHE_KEY;
        let abilities = compiledAbilitiesCache.get(cacheKey);

        if (!abilities) {
            const roleAbilities = roles.length ?
                (await Promise.all(roles.map(role => fetchAbilitiesForRole(ctx, role)))).flat() :
                await fetchAbilitiesForRole(ctx);

            abilities = new PureAbility(
                unpackRules(roleAbilities),
                {
                    detectSubjectType,
                    conditionsMatcher
                }
            );

            compiledAbilitiesCache.set(cacheKey, abilities);
        }

        return abilities;
    }

    /**
     * Get the tenant IDs that the current session has access to based on the roles.
     * If a filter is specified, only the tenant IDs for the specified roles are returned.
     * Duplicate tenant IDs are removed from the resulting array.
     *
     * @param {Object} roles - The roles for the current session.
     * @param {Array} rolesFilter - The roles to filter the tenant IDs by. If not specified, all tenant IDs are returned.
     * @returns {Array} - Returns an array of tenant IDs.
     */
    function getTenantIdsForRoles(roles, rolesFilter = null) {
        if (rolesFilter === null) {
            return Object.values(roles)
                .flat()
                .filter((id, i, arr) => arr.indexOf(id) === i);
        }

        if (!Array.isArray(rolesFilter)) {
            rolesFilter = [ rolesFilter ];
        }

        return rolesFilter.reduce((acc, role) => {
            if (Array.isArray(roles[role])) {
                acc.push(...roles[role]);
            }
            return acc;
        }, []).filter((id, i, arr) => arr.indexOf(id) === i);
    }

    /**
     * Fetches the roles for an application, user or the default roles from the cache or the service.
     *
     * @param {Object} ctx - The context object for the action.
     * @param {String} cacheKey - The cache key for the roles.
     * @param {String} action - The name of the action to call to fetch the roles.
     * @param {String} paramName - The name of the parameter containing the ID to fetch the roles for.
     * @param {String} id - The ID of the application or user to fetch the roles for.
     * @param {String} secret - An optional secret to send with the request.
     * @returns {Promise} - Returns a promise that resolves with the roles for the application, user or the default roles.
     */
    async function fetchRoles(ctx, cacheKey, action, paramName = null, id = null, secret = null) {
        let roles = entitiesRolesCache.get(cacheKey);

        if (!roles) {
            const params = {};

            if (id !== null && paramName !== null) {
                params[paramName] = id;
            }

            if (secret !== null) {
                params.secret = secret;
            }

            roles = await ctx.broker.call(action, params);
            entitiesRolesCache.set(cacheKey, roles);
        }

        return roles;
    }

    /**
     * Fetches the roles for an application.
     *
     * @param {String} id - The ID of the application to fetch the roles for.
     * @param {String} id - The ID of the application to fetch the roles for.
     * @returns {Promise} - Returns a promise that resolves with the roles for the application.
     */
    function getRolesForApplication(ctx, id) {
        return fetchRoles(
            ctx,
            `${CACHE_KEY_APPLICATION}:${id}`,
            opts.applicationRolesAction,
            opts.applicationRolesActionParam,
            id,
            opts.applicationRolesActionSecret
        );
    }

    /**
     * Fetches the roles for a user.
     *
     * @param {Object} ctx - The context object for the action.
     * @param {String} id - The ID of the user to fetch the roles for.
     * @returns {Promise} - Returns a promise that resolves with the roles for the user.
     */
    function getRolesForUser(ctx, id) {
        return fetchRoles(
            ctx,
            `${CACHE_KEY_USER}:${id}`,
            opts.userRolesAction,
            opts.userRolesActionParam,
            id,
            opts.userRolesActionSecret
        );
    }

    /**
     * Fetches the default roles.
     *
     * @param {Object} ctx - The context object for the action.
     * @returns {Promise} - Returns a promise that resolves with the default roles.
     */
    function getRolesForAnonymous(ctx) {
        if (opts.anonymousRolesAction === null) {
            return Promise.resolve({});
        }

        return fetchRoles(ctx, CACHE_KEY_ANONYMOUS, opts.anonymousRolesAction);
    }

    /**
     * Fetches the role for the current session.
     *
     * @param {Object} ctx - The context object for the action.
     * @param {Object} authn - The authentication object for the current session.
     * @returns {Promise} - Returns a promise that resolves with an object containing the roles for the current session.
     */
    function getRolesForAuth(ctx, authn = {}) {
        const kind = authn.kind || null;

        switch (kind) {
            case 'application':
                return getRolesForApplication(ctx, authn.id);

            case 'user':
                return getRolesForUser(ctx, authn.id);

            case 'anonymous':
                return getRolesForAnonymous(ctx);

            default:
                logger.warn(`Unknown authentication kind ${kind}`, authn);
                throw new MoleculerClientError('Unknown authentication kind', 500, 'ERR_UNKNOWN_AUTH_KIND', { kind });
        }
    }

    /**
     * Builds the authorization context for the current session.
     * This includes the roles, abilities and methods to check the ACLs.
     *
     * @param {Object} ctx - The context object for the action.
     * @returns {Promise} - Returns a promise that resolves with the authorization context for the current session.
     */
    async function buildAuthorizationContext(ctx) {
        const roles = await getRolesForAuth(ctx, ctx.locals?.authn || {});
        const abilities = await getAbilitiesForRoles(ctx, Object.keys(roles));

        const manualACLs = ctx.action?.authManualACL || false;
        let ACLChecks = 0;

        Object.freeze(roles);

        const authz = {
            /**
             * @returns {Object} - Returns an object containing all the tenant IDs indexed by role name that the current session has access to.
             */
            getRoles() {
                return roles;
            },

            /**
             * @returns {Array} - Returns an array of all the role names that the current session has access to.
             */
            getRoleNames() {
                return Object.keys(roles);
            },

            /**
             * @param {Array} filters - The roles to filter the tenant IDs by. If not specified, all tenant IDs are returned.
             * @returns {Array} - Returns an array of tenant IDs that the current session has access to.
             */
            getTenantIds(filters) {
                return getTenantIdsForRoles(roles, filters);
            },

            /**
             * Checks if the current session has the specified role or roles for the specified tenant.
             * If an array of tenant IDs is specified, it checks if the current session has the role for all the tenants.
             *
             * @param {String} role - The name of the role to check.
             * @param {String|Array} tenant_id - The ID of the tenant to check the role for.
             * @returns {Boolean} - Returns true if the current session has the role.
             */
            hasRole(role = null, tenant_id = null) {
                if (role === null || !Array.isArray(roles[role])) {
                    return false;
                }

                if ((typeof tenant_id !== 'string' && !Array.isArray(tenant_id)) || !tenant_id.length) {
                    throw new Error(`tenant_id is not a valid string or array, got ${tenant_id}`);
                }

                if (Array.isArray(tenant_id)) {
                    // Check if all tenant ids are present in the token
                    return tenant_id.every(id => roles[role].includes(id));
                }

                return roles[role].includes(tenant_id);
            },

            /**
             * Checks if the current session can perform the given action on the given object.
             *
             * @param {String} action - The name of the action to check.
             * @param {Object} object - The object to check the action on.
             * @returns {Boolean} - Returns true if the current session can perform the action on the object.
             * @throws {MoleculerClientError} - Throws a MoleculerClientError if the current session cannot perform the action on the object.
             */
            can(action, object) {
                ACLChecks += 1;
                return abilities.can(action, object);
            },

            /**
             * Returns the rules for the given action and subject.
             *
             * @param {String} action - The name of the action to get the rules for.
             * @param {Object} subject - The object to get the rules for.
             * @returns {Array} - Returns an array of rules for the action and object.
             */
            rulesFor(action, subject) {
                return abilities.rulesFor(action, subject);
            },

            /**
             * @throws {MoleculerClientError} - Throws a MoleculerClientError if the ACL checks were not performed.
             */
            throwIfACLNotChecked() {
                if (!manualACLs && !ACLChecks) {
                    // Should never happen and there is no real way to test for this
                    /* istanbul ignore next */
                    ctx.service.logger.fatal(`Action ${ctx.action.name} called without any ACL checks!`, ctx.params);

                    /* istanbul ignore next */
                    throw new MoleculerClientError('Forbidden', 403, 'ERR_FORBIDDEN', {
                        message: 'Failed to check ACLs'
                    });
                }

                // If the action has `authManualACL` set to true, the ACL check is expected to be done manually
                // If the action has `authManualACL` set to a number, the ACL check is expected to be done at least that many times
                if ((manualACLs === true && !ACLChecks) || (typeof manualACLs === 'number' && ACLChecks < manualACLs)) {
                    ctx.service.logger.fatal(`Action ${ctx.action.name} called without the expected number of ACL checks!`, ctx.params);
                    ctx.service.logger.info(`Expected ${manualACLs} checks, got ${ACLChecks}`);

                    throw new MoleculerClientError('Forbidden', 403, 'ERR_FORBIDDEN', {
                        message: 'Failed to check ACLs'
                    });
                }
            }
        };

        return authz;
    }

    /**
     * Registers the authorization methods on the context object.
     * Freezes the authorization object to prevent modifications.
     * Defines the authorization object as a read-only property on the context.
     *
     * @param {Object} ctx - The context object for the action.
     * @param {Object} authz - The authorization object for the current session.
     */
    function registerAuthzMethods(ctx, authz) {
        Object.freeze(authz);
        Object.defineProperty(ctx.locals, 'authz', {
            value: authz,
            writable: false,
            enumerable: true
        });
    }

    /**
     * Registers deprecated properties on the context object.
     *
     * @param {Object} ctx - The context object for the action.
     */
    function registerDeprecatedProperties(ctx) {
        // Keep `setSubject()` for backward compatibility
        if (ctx.locals.setSubject !== undefined) {
            delete ctx.locals.setSubject;
        }
        Object.defineProperty(ctx.locals, 'setSubject', {
            value: (subj, params) => {
                logger.warn('`ctx.locals.setSubject()` is deprecated, import casl\'s `subject` directly instead');
                return subject(subj, params);
            },
            writable: false,
            enumerable: true
        });

        if (ctx.locals.abilities !== undefined) {
            delete ctx.locals.abilities;
        }
        const abilities = {
            can: (action, subject) => {
                logger.warn('`ctx.locals.abilities.can()` is deprecated, use `ctx.locals.authz.can()` instead');
                return ctx.locals.authz.can(action, subject);
            },
            rulesFor: (action, subject) => {
                logger.warn('`ctx.locals.abilities.rulesFor()` is deprecated, use `ctx.locals.authz.rulesFor()` instead');
                return ctx.locals.authz.rulesFor(action, subject);
            }
        };

        Object.defineProperty(ctx.locals, 'abilities', {
            value: abilities,
            writable: false,
            enumerable: true
        });
    }

    /**
     * Creates the event handlers for the middleware.
     * If no events are specified, returns null.
     */
    function makeEventsHandlers() {
        const events = {};

        if (typeof opts.abilitiesChangedEvent === 'string' && opts.abilitiesChangedEvent.length > 0) {
            events[opts.abilitiesChangedEvent] = {
                params: { roleName: 'string|optional' },

                handler(ctx) {
                    if (ctx.params && ctx.params.roleName) {
                        logger.debug(`Removing abilities cache for role "${ctx.params.roleName}"`);
                        roleAbilitiesCache.delete(ctx.params.roleName);
                    } else {
                        logger.debug('Clearing abilities cache');
                        roleAbilitiesCache.clear();
                    }

                    logger.debug('Clearing compiled abilities cache');
                    compiledAbilitiesCache.clear();
                }
            };
        }

        if (typeof opts.applicationRolesChangedEvent === 'string' && opts.applicationRolesChangedEvent.length > 0) {
            events[opts.applicationRolesChangedEvent] = {
                params: { appId: 'string|optional' },

                handler(ctx) {
                    if (ctx.params && ctx.params.appId) {
                        logger.debug(`Removing application roles cache for ID "${ctx.params.appId}"`);
                        entitiesRolesCache.delete(`${CACHE_KEY_APPLICATION}:${ctx.params.appId}`);
                    } else {
                        logger.debug('Clearing application roles cache');
                        entitiesRolesCache.clear();
                    }
                }
            };
        }

        if (typeof opts.userRolesChangedEvent === 'string' && opts.userRolesChangedEvent.length > 0) {
            events[opts.userRolesChangedEvent] = {
                params: { userId: 'string|optional' },

                handler(ctx) {
                    if (ctx.params && ctx.params.userId) {
                        logger.debug(`Removing user roles cache for ID "${ctx.params.userId}"`);
                        entitiesRolesCache.delete(`${CACHE_KEY_USER}:${ctx.params.userId}`);
                    } else {
                        logger.debug('Clearing user roles cache');
                        entitiesRolesCache.clear();
                    }
                }
            };
        }

        if (typeof opts.anonymousRolesChangedEvent === 'string' && opts.anonymousRolesChangedEvent.length > 0) {
            events[opts.anonymousRolesChangedEvent] = () => {
                logger.debug('Clearing anonymous roles cache');
                entitiesRolesCache.delete(CACHE_KEY_ANONYMOUS);
            };
        }

        return Object.keys(events).length ? events : null;
    }

    const authorizationMiddleware = {
        name: 'Authorization',

        /**
         * Validates the options for the middleware and get the logger.
         */
        created(broker) {
            if (typeof opts.abilitiesForRoleAction !== 'string') {
                throw new ServiceSchemaError('The `abilitiesForRoleAction` option must be a string');
            }

            if (typeof opts.defaultAbilitiesAction !== 'string') {
                throw new ServiceSchemaError('The `defaultAbilitiesAction` option must be a string');
            }

            if (typeof opts.applicationRolesAction !== 'string') {
                throw new ServiceSchemaError('The `applicationRolesAction` option must be a string');
            }

            if (typeof opts.userRolesAction !== 'string') {
                throw new ServiceSchemaError('The `userRolesAction` option must be a string');
            }

            if (typeof opts.anonymousRolesAction !== 'string' && opts.anonymousRolesAction !== null) {
                throw new ServiceSchemaError('The `anonymousRolesAction` option must be a string or be null');
            }

            if (typeof opts.entitiesRolesCacheMax !== 'number') {
                throw new ServiceSchemaError('The `entitiesRolesCacheMax` option must be a number');
            }

            if (typeof opts.entitiesRolesCacheTTL !== 'string') {
                throw new ServiceSchemaError('The `entitiesRolesCacheTTL` option must be a string');
            }

            if (typeof opts.roleAbilitiesCacheMax !== 'number') {
                throw new ServiceSchemaError('The `roleAbilitiesCacheMax` option must be a number');
            }

            if (typeof opts.roleAbilitiesCacheTTL !== 'string') {
                throw new ServiceSchemaError('The `roleAbilitiesCacheTTL` option must be a string');
            }

            if (typeof opts.compiledAbilitiesCacheMax !== 'number') {
                throw new ServiceSchemaError('The `compiledAbilitiesCacheMax` option must be a number');
            }

            if (typeof opts.compiledAbilitiesCacheTTL !== 'string') {
                throw new ServiceSchemaError('The `compiledAbilitiesCacheTTL` option must be a string');
            }

            logger = opts.logger || broker.getLogger('Authorization');

            const events = makeEventsHandlers();

            if (!events) {
                return;
            }

            broker.createService({
                name: opts.eventsServiceName,
                events
            });
        },

        /**
         * Initializes the caches for the middleware.
         */
        starting() {
            entitiesRolesCache = new LRUCache({
                max: opts.entitiesRolesCacheMax,
                ttl: ms(opts.entitiesRolesCacheTTL)
            });

            roleAbilitiesCache = new LRUCache({
                max: opts.roleAbilitiesCacheMax,
                ttl: ms(opts.roleAbilitiesCacheTTL)
            });

            compiledAbilitiesCache = new LRUCache({
                max: opts.compiledAbilitiesCacheMax,
                ttl: ms(opts.compiledAbilitiesCacheTTL)
            });
        },

        /**
         * Clears the caches when the service is stopped.
         */
        stopped() {
            if (entitiesRolesCache !== null) {
                entitiesRolesCache.clear();
            }

            if (roleAbilitiesCache !== null) {
                roleAbilitiesCache.clear();
            }

            if (compiledAbilitiesCache !== null) {
                compiledAbilitiesCache.clear();
            }
        },

        /**
         * Local actions hook
         *
         * @param {Function} next - The next function in the middleware chain.
         * @param {Object} action - The action object.
         * @param {Boolean} action.disableAuthorization - If true, the middleware will skip the authorization.
         * @param {Boolean} action.authManualACL - If true, the ACL check is expected to be done manually.
         * @param {Number} action.authManualACL - If a number, the ACL check is expected to be done at least that many times.
         * @returns {Function} - The middleware function for the action.
         * @throws {MoleculerServerError} - Throws a MoleculerServerError if the context ID is undefined.
         */
        localAction(next, action) {
            if (action.disableAuthorization || opts.serviceBlacklist.includes(action.service.name)) {
                return next;
            }

            return async (ctx) => {
                logger.debug(`Authorization middleware for action '${action.name}'`);

                if (typeof ctx.locals.authn !== 'object' || typeof ctx.locals.authn.kind !== 'string') {
                    logger.error('No authentication middleware found, unable to check ACLs', ctx);
                    throw new MoleculerServerError('Unable to check ACLs', 500, 'ERR_UNABLE_TO_CHECK_ACLS');
                }

                const authz = await buildAuthorizationContext(ctx);

                registerAuthzMethods(ctx, authz);
                registerDeprecatedProperties(ctx);

                // Automatically check the ACL for this action.
                if (!action.authManualACL) {
                    await assertAbilityToCallAction(ctx, action);
                }

                // Execute the action and return the result.
                return next(ctx)
                    .then(res => {
                        // Throw an error if the ACL was not checked and discard the result.
                        authz.throwIfACLNotChecked(ctx);
                        return res;
                    });
            };
        },

        /**
         * Methods
         */
        methods: {
            /**
             * @deprecated - Use `ctx.locals.authz.getTenantIds()` instead.
             * Checks if the current session has the specified role.
             *
             * @param {String} role - The name of the role to check.
             * @param {String} tenantId - The ID of the tenant to check the role for.
             * @returns {Boolean} - Returns true if the current session has the role.
             */
            getTenantIds: (ctx, rolesFilter) => {
                logger.warn('`getTenantIds()` is deprecated, use `ctx.locals.authz.getTenantIds()` instead');
                return ctx.locals.authz.getTenantIds(rolesFilter);
            }
        }
    };

    return authorizationMiddleware;
};
