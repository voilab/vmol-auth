'use strict';

const ms = require('ms');
const { MoleculerClientError, MoleculerError, ServiceSchemaError } = require('moleculer').Errors;
const jwt = require('jsonwebtoken');
const { LRUCache } = require('lru-cache');
const TTLCache = require('@isaacs/ttlcache');

const JWT_PARAMS_CACHE_KEY = 'jwtParams';

/**
 * The authentication middleware.
 * It verifies the user and application tokens and sets the authentication info in the context.
 *
 * @param {Object} opts - The options for the middleware.
 * @param {string} opts.userJWTParamsAction - Name of the action to call to get the JWT parameters for the user tokens.
 * @param {string} opts.userJWTParamsChangeEvent - Name of the event to listen for changes in the JWT parameters.
 * @param {string} opts.appAuthGetPublicKeyAction - Name of the action to call to get the public key of an application.
 * @param {string} opts.appAuthPublicKeyChangeEvent - Name of the event to listen for changes in the application public keys.
 * @param {number} opts.jwtClockTolerance - Clock tolerance for JWT verification in seconds. Defaults to 30 seconds.
 * @param {string} opts.jwtParamsTTL - Time-to-live for the JWT parameters cache. Defaults to 1 hour.
 * @param {number} opts.applicationKeysCacheSize - Maximum size of the application keys cache. Defaults to 128.
 * @param {string} opts.applicationKeysCacheTTL - Time-to-live for the application keys cache. Defaults to 1 day.
 * @param {number} opts.applicationJwtClockTolerance - Clock tolerance for application JWT verification in seconds. Defaults to 30 seconds.
 * @param {string} opts.applicationJwtMaxAge - Maximum age for application JWT verification. Defaults to 30 minutes.
 * @param {string} opts.slugSeparator - Separator for the authentication slug. Defaults to `:`.
 * @param {string} opts.eventsServiceName - Name of the service listening for events. Defaults to `AuthenticationEvents`.
 * @param {string[]} opts.serviceBlacklist - List of services to skip the authentication middleware. Defaults to `[]`. The `$node` service is always added to the blacklist.
 */
module.exports = function AuthenticationMiddleware(opts) {
    opts = Object.assign({}, {
        userJWTParamsAction: 'authentication.getJWTParams',
        userJWTParamsChangeEvent: null,

        appAuthGetPublicKeyAction: 'authentication.getApplicationPublicKey',
        appAuthPublicKeyChangeEvent: null,

        jwtClockTolerance: 30,
        jwtParamsTTL: '1h',

        applicationKeysCacheSize: 128,
        applicationKeysCacheTTL: '1d',
        applicationJwtClockTolerance: 30,
        applicationJwtMaxAge: '30m',

        slugSeparator: ':',

        eventsServiceName: 'AuthenticationEvents',

        serviceBlacklist: []
    }, opts);

    opts.serviceBlacklist.push('$node');

    /**
     * @type {TTLCache} - Cache for JWT parameters.
     */
    let jwtParamsCache = null;

    /**
     * @type {LRUCache} - Cache for application keys.
     */
    let applicationKeysCache = null;

    /**
     * @type {Object} - The logger for the middleware.
     */
    let logger = null;

    /**
     * Fetches JWT parameters from the cache or the auth service's settings.
     *
     * @param {Object} ctx - The context object.
     * @returns {Promise<Object>} - A promise that resolves to the JWT parameters.
     * @throws {MoleculerError} - If JWT config is not found in the specified service.
     */
    async function fetchJwtParams(ctx) {
        if (jwtParamsCache.has(JWT_PARAMS_CACHE_KEY)) {
            return jwtParamsCache.get(JWT_PARAMS_CACHE_KEY);
        }

        return ctx.broker.call(opts.userJWTParamsAction)
            .then(params => {
                if (!params || !Object.keys(params).length) {
                    throw new MoleculerError(`JWT config not found in action "${opts.userJWTParamsAction}"`, 500, 'ERR_JWT_CONFIG');
                }

                jwtParamsCache.set(JWT_PARAMS_CACHE_KEY, params);
                return params;
            });
    }

    /**
     * Fetches the public key for the specified application from the cache or makes a broker call to retrieve it.
     *
     * @param {Object} ctx - The context object.
     * @param {string} appName - The name of the application.
     * @returns {Promise<string>} - A promise that resolves with the public key of the application.
     */
    function fetchAppPublicKey(ctx, appName) {
        if (applicationKeysCache.has(appName)) {
            return Promise.resolve(applicationKeysCache.get(appName));
        }

        return ctx.broker.call(
            opts.appAuthGetPublicKeyAction,
            { issuer: appName }
        )
            .then(appPublicKey => {
                applicationKeysCache.set(appName, appPublicKey);
                return appPublicKey;
            });
    }

    /**
     * Handle token exception.
     *
     * @param {Error} e - The token exception error.
     * @throws {MoleculerClientError} - Throws a MoleculerClientError with specific error details.
     * @returns {void}
     */
    function handleTokenException(e) {
        if (e instanceof jwt.TokenExpiredError) {
            throw new MoleculerClientError('Expired token', 401, 'TOKEN_EXPIRED_ERROR', { message: `Auth: ${e.message}` });
        }

        if (e instanceof jwt.JsonWebTokenError) {
            throw new MoleculerClientError('Invalid token', 401, 'INVALID_TOKEN_ERROR', { message: `Auth: ${e.message}` });
        }

        // If the error is already a MoleculerClientError, it should be thrown as is.
        if (e instanceof MoleculerClientError) {
            throw e;
        }

        logger.error(`Unknown token error: ${e.message}`);
        throw new MoleculerClientError('General token error', 401, 'TOKEN_GENERIC_ERROR', { message: `Auth: ${e.message}` });
    }

    /**
     * Makes a slug by concatenating all the provided arguments.
     * It uses the `slugSeparator` option to separate the arguments.
     *
     * @param  {...any} args - The arguments to concatenate.
     * @returns {string} - The slug.
     */
    function makeSlug(...args) {
        return args.join(opts.slugSeparator);
    }

    /**
     * Makes a user authentication info object from the provided token.
     *
     * @param {Object} token - The user token.
     * @returns {Object} - The user authentication info object.
     * @property {string} token - The user token.
     * @property {string} kind - The kind of token (user, application or anonymous).
     * @property {string} id - The userID.
     * @property {string} email - The user email.
     * @property {string} sessid - The user session ID.
     * @property {string} slug - The authentication slug.
     */
    function makeUserAuthInfo(token) {
        const kind = 'user';

        const auth = {
            token,
            kind,
            id: token.sub,
            email: token.email,
            sessid: token.ses,
            slug: makeSlug(kind, token.sub, token.ses)
        };

        return auth;
    }

    /**
     * Makes an application authentication info object from the provided token.
     *
     * @param {Object} token - The application token.
     * @returns {Object} - The application authentication info object.
     * @property {string} token - The application token.
     * @property {string} kind - The kind of token (user, application or anonymous).
     * @property {string} id - The application ID.
     * @property {string} slug - The authentication slug.
     */
    function makeApplicationAuthInfo(token) {
        const kind = 'application';

        const auth = {
            token,
            kind,
            id: token.sub,
            slug: makeSlug(kind, token.sub)
        };

        return auth;
    }

    /**
     * Makes an anonymous authentication info object.
     *
     * @returns {Object} - The anonymous authentication info object.
     * @property {string} kind - The kind of token (user, application or anonymous).
     * @property {string} slug - The authentication slug.
     */
    function makeAnonymousAuthInfo() {
        const kind = 'anonymous';

        const auth = {
            kind,
            slug: kind
        };

        return auth;
    }

    /**
     * Verifies the user token using the provided context.
     *
     * @param {Object} ctx - The context object containing the user token.
     * @returns {Promise} - A promise that resolves with the verified user token.
     */
    function verifyUserToken(ctx) {
        return fetchJwtParams(ctx)
            .then(jwtParams => jwt.verify(
                ctx.meta.token,
                jwtParams.publicKey,
                {
                    algorithms: ['ES256', 'ES384', 'ES512'],
                    issuer: jwtParams.issuer,
                    clockTolerance: opts.jwtClockTolerance
                }
            ));
    }

    /**
     * Verifies the application token using the provided context.
     * It fetches the public key for the application and verifies the token.
     *
     * @param {Object} ctx - The context object.
     * @returns {Promise} - A promise that resolves with the verified token.
     */
    function verifyAppToken(ctx) {
        const token = ctx.meta.appToken;
        const infos = jwt.decode(token);

        // Guard against invalid issuers.
        if (!infos || typeof infos.iss !== 'string' || infos.iss.length === 0) {
            return Promise.reject(new MoleculerClientError('Invalid application token', 401, 'INVALID_APP_TOKEN_ERROR', { message: 'Auth: Invalid application token' }));
        }

        return fetchAppPublicKey(ctx, infos.iss)
            .catch((e) => {
                logger.warn(`Error while fetching public key for application "${infos.iss}": ${e.message}`);
                // The application key was likely not found. But we don't want to expose this information to the client.
                throw new MoleculerClientError('Invalid application token', 401, 'INVALID_APP_TOKEN_ERROR', { message: 'Auth: Invalid application token' });
            })
            .then(appPublicKey => jwt.verify(
                token,
                appPublicKey,
                {
                    algorithms: ['ES256', 'ES384', 'ES512'],
                    issuer: infos.issuer,
                    maxAge: opts.applicationJwtMaxAge,
                    clockTolerance: opts.applicationJwtClockTolerance
                }
            ));
    }

    /**
     * Verifies the token in the provided context.
     * If a user token is present, it will be verified.
     * If an application token is present, it will be verified.
     *
     * @param {Object} ctx - The context object.
     * @returns {Promise} - A promise that resolves with the verified token.
     */
    function makeAuthInfoFromContext(ctx) {
        if (ctx.meta.token) {
            return verifyUserToken(ctx)
                .then(makeUserAuthInfo);
        }

        if (ctx.meta.appToken) {
            return verifyAppToken(ctx)
                .then(makeApplicationAuthInfo);
        }

        return Promise.resolve(makeAnonymousAuthInfo());
    }

    /**
     *
     * @param {Object} ctx - The context object.
     * @param {Object} auth - The authentication info object.
     */
    function setAuthInfoInContext(ctx, auth) {
        const token = auth.token || null;
        delete auth.token;

        // Defines a getter-only property for the deprecated `ctx.locals.token` property.
        if (ctx.locals.token !== undefined) {
            delete ctx.locals.token;
        }
        Object.defineProperty(ctx.locals, 'token', {
            get() {
                logger.warn('The `ctx.locals.token` property is deprecated. Use the `ctx.locals.authn` object instead.');
                return token || null;
            }
        });

        // Defines a getter-only property for the deprecated `ctx.locals.token.roles` property.
        // This is handled by the `getRoles()` method defined in the authorization middleware.
        if (token !== null) {
            // Roles included in the token are ignored.
            if (token.roles !== undefined) {
                delete token.roles;
            }

            Object.defineProperty(token, 'roles', {
                get() {
                    logger.warn('The `ctx.locals.token.roles` property is deprecated. Use the `ctx.locals.authz.getRoles()` method instead.');

                    if (!ctx.locals.authz || !ctx.locals.authz.getRoles) {
                        logger.error('The `ctx.locals.authz` object is not available. The `ctx.locals.authz.getRoles()` method will return an empty object.');
                        return {};
                    }

                    return ctx.locals.authz.getRoles();
                }
            });
        }

        Object.freeze(auth);
        Object.defineProperty(ctx.locals, 'authn', {
            value: auth,
            writable: false,
            enumerable: true
        });
    }

    /**
     * Makes the event handlers for the authentication events.
     * If no event names are provided, returns `null`.
     */
    function makeEventsHandlers() {
        const events = {};

        if (typeof opts.userJWTParamsChangeEvent === 'string' && opts.userJWTParamsChangeEvent.length > 0) {
            events[opts.userJWTParamsChangeEvent] = () => {
                logger.debug('Clearing the JWT parameters cache');
                jwtParamsCache.clear();
            };
        }

        if (typeof opts.appAuthPublicKeyChangeEvent === 'string' && opts.appAuthPublicKeyChangeEvent.length > 0) {
            events[opts.appAuthPublicKeyChangeEvent] = {
                params: { appName: 'string|optional' },

                handler(ctx) {
                    if (ctx.params && ctx.params.appName) {
                        logger.debug(`Removing application key for "${ctx.params.appName}" from the cache`);
                        applicationKeysCache.delete(ctx.params.appName);
                    } else {
                        logger.debug('Clearing the application keys cache');
                        applicationKeysCache.clear();
                    }
                }
            };
        }

        return Object.keys(events).length > 0 ? events : null;
    }

    const authenticationMiddleware = {
        name: 'Authentication',

        /**
         * Validates the options when the broker is created.
         */
        created(broker) {
            if (typeof opts.userJWTParamsAction !== 'string' || opts.userJWTParamsAction.length === 0) {
                throw new ServiceSchemaError('The `userJWTParamsAction` option must be a non-empty string on the authentication middleware!');
            }

            if (typeof opts.appAuthGetPublicKeyAction !== 'string' || opts.appAuthGetPublicKeyAction.length === 0) {
                throw new ServiceSchemaError('The `appAuthGetPublicKeyAction` option must be a non-empty string on the authentication middleware!');
            }

            logger = opts.logger || broker.getLogger('authentication');

            const events = makeEventsHandlers();

            if (!events) {
                return;
            }

            broker.createService({
                name: opts.eventsServiceName,
                events
            });
        },

        /**
         * Initializes the cache for JWT parameters and application keys when the service is created.
         */
        starting() {
            jwtParamsCache = new TTLCache({
                ttl: ms(opts.jwtParamsTTL)
            });

            applicationKeysCache = new LRUCache({
                max: opts.applicationKeysCacheSize,
                maxAge: ms(opts.applicationKeysCacheTTL)
            });
        },

        /**
         * Clears the cache for JWT parameters and application keys when the service is stopped.
         */
        stopped() {
            if (jwtParamsCache !== null) {
                jwtParamsCache.clear();
            }

            if (applicationKeysCache !== null) {
                applicationKeysCache.clear();
            }
        },

        /**
         * Middleware for local actions.
         * It verifies the token and sets the user or application token in the context.
         * If the action has the `disableAuthentication` flag set to `true` or
         * the action's service is in the blacklist, the middleware is skipped.
         *
         * @param {Function} next - The next middleware function.
         * @param {Object} action - The action object.
         * @returns {Function} - The middleware function for the action.
         */
        localAction(next, action) {
            if (action.disableAuthentication || opts.serviceBlacklist.includes(action.service.name)) {
                return next;
            }

            return (ctx) => {
                logger.debug(`Authentication middleware for action '${action.name}'`);

                return makeAuthInfoFromContext(ctx)
                    .then(auth => {
                        setAuthInfoInContext(ctx, auth);
                    })
                    .catch(handleTokenException)
                    .then(() => next(ctx));
            };
        }
    };

    return authenticationMiddleware;
};
